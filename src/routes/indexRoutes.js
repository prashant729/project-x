import Layout from '../layout.js';

const indexRoutes = [{ path: "/", component: Layout }];

export default indexRoutes;