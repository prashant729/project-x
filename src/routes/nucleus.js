import {lazy} from 'react';
import WithSuspense from '../components/common/HOC/withSuspense';

const Login = lazy(()=>import('../components/login'));
const ErrorPage = lazy(()=>import('../components/errorPage'));
const Permissions = lazy(()=>import('../components/permissions'));
const Dashboard = lazy(()=>import('../components/dashboard/'))
const LoadingData = lazy(()=>import('../components/addData/loadingData/'))
const UnloadingData = lazy(()=>import('../components/addData/unloadingData/'))
const IndentPlacement = lazy(()=>import('../components/addData/indentPlacement/'))
const RakeAssignment = lazy(()=>import('../components/addData/rakeAssignment/'))
const RakePosition = lazy(()=>import('../components/addData/rakePosition/'))
const SignUp = lazy(()=>import('../components/signup/'))
const SidingDetails = lazy(()=>import('../components/sidingDetails/'))


var nucleusRoutes = [
	{
		path: "/dashboard",
		sidebarName: "Dashboard",
		restriction: false,
	    component: WithSuspense(Dashboard)
	},
	{
		path: "/addData",
		sidebarName: "Add Data",
		subMenu: [
			{
				name: "Loading Data",
				path: "/loadingData",
	    		component: WithSuspense(LoadingData)
			},
			{
				name: "Unloading Data",
				path: "/unloadingData",
	    		component: WithSuspense(UnloadingData)
			},
			{
				name: "Indent Placement",
				path: "/indentPlacement",
	    		component: WithSuspense(IndentPlacement)
			},
			{
				name: "Rake Assignment",
				path: "/rakeAssignment",
	    		component: WithSuspense(RakeAssignment)
			},
			{
				name: "Rake Position",
				path: "/rakePosition",
	    		component: WithSuspense(RakePosition)
			}
		],
		restriction: false,
	    component: null
	},
	{
		path: "/signup",
		sidebarName: "Sign Up",
		restriction: false,
	    component: WithSuspense(SignUp)
	},
	{
		path: '/sidingDetails',
		sidebarName: "Siding Details",
		restriction: false,
		component: WithSuspense(SidingDetails)
	},
	{
		path: "/error",
		sidebarName: "ErrorPage",
		restriction: true,
	    component: WithSuspense(ErrorPage)
	},
	{
		path: "/login",
		sidebarName: "Login",
		restriction: true,
	    component: WithSuspense(Login)
	}
]

export default nucleusRoutes
