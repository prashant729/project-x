import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import nucleusRoutes from "./routes/nucleus.js"
import Header from './components/header'
const {sessionStorage} = window;

export default class App extends React.Component {
    render() {
        const switchRoutes = (
          <Switch>
            {
                nucleusRoutes.map((prop, key) => {
                    let isLoggedIn = sessionStorage.getItem('refreshToken') === null ? false : true;
                    if (prop.path === '/login') {
                        return <Route path={prop.path} component={prop.component} key={key} />
                    } else if (prop.redirect) {
                        return <Redirect from={prop.path} to={prop.to} key={key} />;
                    } else if (isLoggedIn === false) {
                        return <Redirect from={prop.path} to='/login' key={key} /> 
                    } else if (prop.subMenu) {
                        return prop.subMenu.map((submenu, key) => {
                            return <Route path={submenu.path} component={submenu.component} key={key} />
                        });
                    }  else {
                        return <Route path={prop.path} component={prop.component} key={key} />
                    }
                })
            }
          </Switch>
        );
        if (window.location.href.split("/")[3] !== 'login'){
            // show header
            return(
                <div>
                    <Header />
                    <div>{switchRoutes}</div>
                </div>
                )
        } else {
            // dont show header
            return(
                <div>{switchRoutes}</div>
                )
        }
    }
}