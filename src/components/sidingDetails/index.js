import React from 'react';
import { observer } from 'mobx-react';

const SidingDetails = observer(class SidingDetails extends React.Component {

	constructor(props){
        super(props);
    }

    render() {
    	return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">SidingDetails</div>
                </div>
                <div className="container">
  					
    			</div>
                <div className="clearfix" />
            </div>
        );
    }
})

export default SidingDetails;
