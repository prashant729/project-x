import React from 'react';
import Dialog from 'material-ui/Dialog';
import PropTypes from 'prop-types';
import { noop } from 'underscore';
import {Button} from '../../atoms';
import './style.css';

const modalStyle = {padding:'24px'};

const Modal = ({closePopup, children, primaryLbel, secondaryLabel,...rest}) => {
    const actions = [
        <Button
            primaryText={secondaryLabel}
            primary={false}
            onClick={closePopup('secondary')}
        />,
        <Button
            primaryText={primaryLbel}
            primary={true}
            onClick={closePopup('primary')}
        />,
    ];
    return(
        <Dialog
            bodyStyle={modalStyle}
            modal={false}
            titleClassName="modal-title"
            actions={actions}
            onRequestClose={closePopup('default')}
            autoScrollBodyContent={true}
            {...rest}
        >
            {children}
        </Dialog>
    )
}

Modal.defaultProps={
    closePopup:noop,
    primaryLbel:'Submit',
    secondaryLabel:'Cancel'
}

Modal.propTypes={
    closePopup: PropTypes.func.isRequired,
    children: PropTypes.element
}

export default Modal;
