import React from 'react';
import ReactTable, { ReactTableDefaults } from 'react-table';
import "react-table/react-table.css";
import './style.css';

const Table = React.memo(({...allProps})=>(
  <ReactTable
    {...allProps}
    column={ReactTableDefaults.column}
  />
));

export default Table;
