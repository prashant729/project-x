import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const getMenuItems = (config,data,values) => (
  data.map((eachItem)=>(
    <MenuItem
      key={eachItem[config.setValueBy]}
      insetChildren={true}
      checked={values.indexOf(eachItem[config.setValueBy]) > -1}
      value={eachItem[config.setValueBy]}
      primaryText={eachItem[config.displayTextBy]}
    />
  ))
);

 const MultipleSelect = React.memo(({data,value,config,...rest}) =>(
   <SelectField
     multiple={true}
     value={value}
     {...rest}
   >
     {getMenuItems(config,data, value)}
   </SelectField>
 ));

 export default MultipleSelect;
