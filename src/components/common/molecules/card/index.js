import React from 'react';
import {noop} from 'lodash';
import {CardButton, Loader, Img, Dropdownfield} from '../../atoms/';
import './style.css';
import '../../styles.css';
import Src from './iron-man.jpg';

const dropDownConfig = {
  displayTextBy:'name',
  setValueBy:'id'
}

const DropDownStyle={
  fontStyle:{
    fontSize:'12px'
  }
}

class RenderByType{
  static Button = (descriptionArr) =>(
    descriptionArr.map((obj, index)=>(
      <span className={`button-view m-l-5 ${obj.isActive && 'active'}`} key={index}>
        {obj.name}
      </span>
    ))
  );
  static Text = (description) =>(
    <span className='description' title={description}>{` ${description}`}</span>
  )
  static DropDown = (description) =>(
  <div className='pull-right dropDown'>
    <Dropdownfield
      items={description}
      config={dropDownConfig}
      value={description[0].id}
      menuItemStyle={DropDownStyle.fontStyle}
      labelStyle={DropDownStyle.fontStyle}
    />
  </div>
  )
}

const renderDetails= (arr) =>(
  arr.map((eachItem,index)=>(
    <React.Fragment key={eachItem.label}>
      <div className='card-label center-y m-t-10 full-width'>
        <span className='label-text'>{`${eachItem.label} : `}</span>
        {RenderByType[eachItem.type](eachItem.description)}
      </div>
    </React.Fragment>
  ))
)


const Card = ({title,img, cardObj, buttonArr, onButtonClick}) => (
  <div className='card-wrapper'>
    <div className="img-holder">
      <React.Suspense fallback={<Loader/>}>
        <Img className='full-width full-height' alt='' src={img || Src}/>
      </React.Suspense>
    </div>
    <div className="description-wrapper p-20">
      <div className='title bold'>{title}</div>
      {renderDetails(cardObj.ostArr)}
    </div>
    <div className='button-group' onClick={onButtonClick}>
      {cardObj.buttonArr.map(({label,value}, index)=>(
        <React.Fragment key={index}>
          <CardButton label={label} value={value}/>
        </React.Fragment>
      ))}
    </div>
  </div>
);

Card.dafaultProps = {
  title:'',
  img:'',
  cardObj:{
    ostArr:[],
    buttonArr:[]
  },
  onButtonClick:noop,
}


export default Card;
