import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import page from '../../NavigationState';
import {observer} from 'mobx-react';

import './styles.css';

const Locations = observer( class Locations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: 0};
  }

  render(){
    return(
      <div>
        <div>
          <DropDownMenu
            value={this.state.value}
            selectedMenuItemStyle={{color:'orange'}}
            labelStyle={{color:'#424242',lineHeight:'normal',padding:'10px 30px 10px 10px', }}
            style={{marginLeft:'15px',height:'auto',backgroundColor:'white',borderRadius:'5px', marginTop: '20px'}}
            iconStyle={{right: '-8px', top: '-4px',fill:'#78909c'}}
            underlineStyle={{display:'none'}}
            onChange={(event, index, value)=>{page.selectedCenter = value;this.setState({value}); console.log(value)}}
          >
          {
            page.centerList.map((item, index)=>{
              if(index === 0 && page.selectedCenter === null){
                this.setState({index});
                page.selectedCenter = item.id;
              }
              return(
                <MenuItem key={'center'+index} value={index} primaryText={item.city_name+' - '+item.center_name} />
              );
            })
          }
          </DropDownMenu>
        </div>
      </div>
    );
  }
})

export default Locations;