import React from 'react';
import  '../../styles.css';
import './style.css';

const Header = ({label}) => (
  <div className='header full-width p-20 bold'>{label}</div>
);

export default Header;
