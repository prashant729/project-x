import React from 'react';
import Snackbar from 'material-ui/Snackbar';

const CustomSnackbar = (props) =>(
      <Snackbar
          autoHideDuration={4000}
          {...props}
        />
)

export default CustomSnackbar;
