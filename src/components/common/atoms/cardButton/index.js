import React from 'react';
import './style.css';

const CardButton = React.memo(({label,value, ...rest}) => (
  <button className={`card-button ${value}`} value={value} {...rest}>{label}</button>
));

export default CardButton;
