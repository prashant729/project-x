import React from 'react';
import {noop} from 'lodash';
import Button from '../button/';
import Uploader from './upload';
import Constants from '../../constants';
import  './style.css';
import  '../../styles.css';

const buttonStyle = {'backgroundColor':Constants.themeColor, color:'#fff'};

export default class UploadWrapper extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			isEditClicked: false,
		};
	};
  onDrop = (acceptedFiles, rejectedFiles) => {
		const {onUploadStart, onUploadComplete} = this.props;
		 const file = acceptedFiles[0];
		 this.setState({loading:true});
		 onUploadStart(file);
		 const reader =  new FileReader();
		 reader.onloadend = (e) => {
       this.setState({
				 file,
				 loading: false,
				 isEditClicked: false,
			 });
			 onUploadComplete(reader.result);
     };
		 reader.readAsDataURL(file);
   };
	 handleEditClick = () =>{
		 this.setState((prevState)=>({
			 ...prevState,
			 isEditClicked: !prevState.isEditClicked
		 }))
	 }

   render() {
		const {isEditClicked} = this.state;
		const {img} = this.props;
    return (
			<div className="upload-wrapper center-x-y full-width relative">
				<div className="edit-button absolute">
					{img && <Button primaryText={isEditClicked?'Back':'Edit'} style={buttonStyle} onClick={this.handleEditClick}/>}
				</div>
				<Uploader  onDrop={this.onDrop} {...this.props} {...this.state}/>
			</div>
			);
  }
}

UploadWrapper.defaultProps={
	onUploadStart:noop,
	onUploadComplete:noop,
	img:null
}
