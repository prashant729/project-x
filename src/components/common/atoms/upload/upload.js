import React from 'react';
import classNames from 'classnames';
import Dropzone from 'react-dropzone';
import Button from '../button/';
import Constants from '../../constants';
import LoaderHOC from '../../HOC/Loader/';
import  './style.css';
import  '../../styles.css';

const buttonStyle = {'backgroundColor':Constants.themeColor, color:'#fff'};

const Uploader = React.memo(({msg, onDrop, img, isEditClicked}) =>{
  if(img && !isEditClicked){
    return (<img src={img} alt='' className='full-width full-height'/>)
  };
  return (
    <div className="full-width full-height p-50 center-x-y">
    <Dropzone onDrop={onDrop} accept={'image/*'}>
      {({getRootProps, getInputProps, isDragActive}) => {
        return (
          <div
            {...getRootProps()}
            className={classNames('dropzone', {'dropzone--isActive': isDragActive})}
          >
            <div className="full-height full-width">
              {isDragActive?
                <p>Drop image here......</p>:
                <div className="inline-block">
                  <div className='drop-title full-width text-center'>{msg}</div>
                  <div className='text-center full-width'>OR</div>
                  <input {...getInputProps()} />
                  <div className="full-width center-x-y">
                      <Button primaryText={'BROWSE'} style={buttonStyle}/>
                  </div>
                  <div>Image size should be less than 5MB</div>
                </div>
              }
            </div>
          </div>
        )
      }}
    </Dropzone>
    </div>
  )
}
);

Uploader.defaultProps = {
	msg:'Drop image here'
}

export default LoaderHOC('loading')(Uploader);
