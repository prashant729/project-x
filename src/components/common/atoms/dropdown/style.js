import Constants from '../../constants';

export default{
	labelStyle:{
				fontSize: '14px',
				color: '#424242',
        lineHeight: 'normal',
        padding: '10px 30px 5px 0',
	},
	defaultStyle:{
        height: '35px',
        width:'100%',
        fontFamily:'Open Sans, sans-serif',
    },
    iconStyle:{
        right: '-8px',
        top: '-4px',
        fill: '#78909c'
    },
    underlineStyle:{
        margin:0,
        bottom:0
    },
    selectedMenuItemStyle:{
    	color: Constants.themeColor
    }
}
