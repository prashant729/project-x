import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'underscore';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import styles from './style';

class MenuItemByType{
  static number=(num)=>{
    return <MenuItem key={num} value={num} primaryText={num}/>
  }
  static string=(str)=>{
    return <MenuItem key={str} value={str} primaryText={str}/>
  }
  static object=(obj, config)=>{
    return <MenuItem key={obj.id} value={obj[config.setValueBy]} primaryText={obj[config.displayTextBy]} />
  }
}

const getMenuItem = (items, config) => items.map((item, index) => MenuItemByType[typeof(item)](item, config));

const Dropdownfield = ({items, config, style, ...rest})=>(
    <DropDownMenu
        selectedMenuItemStyle={styles.selectedMenuItemStyle}
        labelStyle={styles.labelStyle}
        style={isEmpty(style)?styles.defaultStyle:style}
        iconStyle={styles.iconStyle}
        underlineStyle={styles.underlineStyle}
        {...rest}
    >
        {getMenuItem(items, config)}
    </DropDownMenu>
)

Dropdownfield.defaultProps={
    items:[],
    style:{},
    name:'',
    config:{}
}

Dropdownfield.propTypes={
    items: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
    styles: PropTypes.object,
    config: PropTypes.object,
}

export default Dropdownfield;
