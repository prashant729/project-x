import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import PropTypes from 'prop-types';

const Button = ({primaryText, ...rest}) => (
    <FlatButton label={primaryText} {...rest} />
);


Button.defaultProps={
    primaryText:'',
    type:''
}

Button.propTypes={
    primaryText: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
}

export default Button;
