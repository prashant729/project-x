import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import  '../../styles.css';

export default React.memo((props)=>(
    <div className="full-width full-height center-x-y">
        <CircularProgress size={60} thickness={4} {...props}/>
    </div>
));
