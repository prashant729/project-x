import React from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import './style.css';

const AddButton = React.memo((props) => (
  <FloatingActionButton className='add-action-button' {...props}>
    <ContentAdd/>
  </FloatingActionButton>
));

export default AddButton;
