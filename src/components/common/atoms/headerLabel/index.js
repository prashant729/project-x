import React from 'react';

const HeaderLabel = React.memo(({label}) => (
  <div className='title-bg'>
    <div className='title-items page-title'>{label}</div>
  </div>
));

export default HeaderLabel;
