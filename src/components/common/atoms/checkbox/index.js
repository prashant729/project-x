import React, { PureComponent } from 'react';
import Checkbox from 'material-ui/Checkbox';

const labelStyle = {fontSize:'14px'};

class CustomCheckbox extends PureComponent {
  handleCheck = () =>{
    const {onChange,value} = this.props;
    onChange(!value);
  }
  render() {
    const {onChange, value,...rest} = this.props;
    return (
      <Checkbox onCheck={this.handleCheck} labelStyle={labelStyle} checked={value} {...rest}/>
    );
  }

}

CustomCheckbox.defaultProps={
  value: false
}

export default CustomCheckbox;
