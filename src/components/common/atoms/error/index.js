import React from 'react';
import ErrorIcon from '@material-ui/icons/Error';
import Constants from '../../constants';
import  '../../styles.css';

export default React.memo(({msg})=>(
    <div className="full-width full-height center-x-y">
      <div className="inline-block">
        <div className='col-md-12 center-x-y'><ErrorIcon/></div>
        <h4 className='text-center col-md-12'>{typeof(msg)==='string'? msg : Constants.defaultError}</h4>
      </div>
    </div>
));
