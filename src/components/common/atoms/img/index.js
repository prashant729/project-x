import React from 'react';
import {unstable_createResource} from 'react-cache';

const ImageResource = unstable_createResource(
  (source)=>(
    new Promise((resolve, reject)=>{
      const img = new Image();
      img.src = source;
      img.onload = resolve;
    })
  )
)

const Img = ({src, alt, ...rest}) => {
  ImageResource.read(src);
  return <img src={src} alt={alt} {...rest}/>
};

export default Img;
