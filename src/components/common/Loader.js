import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

export default (props)=>(
    <div className="vhalign">
        <CircularProgress size={60} thickness={4} {...props}/>
    </div>
)
