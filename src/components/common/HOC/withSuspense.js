import React, {Suspense} from 'react';
import Loader from '../Loader';

export default (Component) => (props={}) => (
	<Suspense fallback={<Loader/>}>
	  <Component {...props}/>
	</Suspense>
);
