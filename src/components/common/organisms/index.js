import LeftPane from './leftPane/';
import RightPane from './rightPane/';

export {
  LeftPane,
  RightPane,
};
