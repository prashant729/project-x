import React from 'react';
import PropTypes from 'prop-types';
import './style.css';
import  '../../styles.css';

const RightPane = React.memo(({children, ...rest}) => (
  <div className='right-pane col-sm-10 p-0' {...rest} >{children}</div>
));

RightPane.propTypes = {
  children:PropTypes.element
}

export default RightPane;
