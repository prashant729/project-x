import React from 'react';
import PropTypes from 'prop-types';
import  '../../styles.css';
import  './style.css';

const LeftPane = React.memo(({children, ...rest}) => (
  <div className='left-pane col-sm-2 p-0' {...rest}>{children}</div>
));

LeftPane.propTypes = {
  children:PropTypes.element
}

export default LeftPane;
