import React from 'react';
import Dialog from 'material-ui/Dialog';


export default class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: props.open || false
        }
    }

    handleClose = () => {
        this.props.onClose && this.props.onClose();
        this.setState({ open: false })
    };

    render() {
        const { title, actions, children } = this.props;
        const { open } = this.state;
        return <Dialog
            title={title}
            actions={actions}
            modal={true}
            open={open}
            onRequestClose={this.handleClose}
        >
            {children}
        </Dialog>
    }
}