import React from 'react';
import { observer } from 'mobx-react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';
import {callDataLevel1, callDataLevel2, callDataLevel3} from "../../API/dashboardCalls";
import './style.css';

const userPerm = JSON.parse(sessionStorage.getItem('userDetails')).accessPermission
const areaOfDuty = JSON.parse(sessionStorage.getItem('userDetails')).areaOfDuty
const userId = JSON.parse(sessionStorage.getItem('userDetails')).userId

const Dashboard = observer(class Dashboard extends React.Component {

	constructor(props){
        super(props);
        this.state = {
            search:"",
            loadingData: [],
            unloadingData: []
        }
    }

    componentDidMount(){
        if (userPerm === '1' || userPerm === '2') {
            callDataLevel1(this.callDataCallback)
        } else if (userPerm === '3') {
            callDataLevel2(this.callDataCallback, areaOfDuty)
        } else if (userPerm === '4'){
            callDataLevel3(this.callDataCallback, userId)
        }
    }

    callDataCallback = (response) => {
    	if (response) {
    		this.setState({loadingData: response.response.dataLoading, unloadingData: response.response.dataUnloading})
    	}
	} 

    render() {
    	const { loadingData, unloadingData } = this.state;
        return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">Dashboard</div>
                </div>
                <div className="container">
  					<div className="text-center">
  						<Input
				            id="adornment-weight"
				            style={{width: '90%'}}
				            aria-describedby="weight-helper-text"
				            endAdornment={<InputAdornment position="end"><SearchIcon /></InputAdornment>}
				            inputProps={{
				              'aria-label': 'Search',
				            }}
				            placeholder="Search RR Number"
				        />
  					</div>
  					<br />
  					<div className="clearfix" />
    				<Card className="card">
      					<CardContent>
        					<Typography variant="h5" component="h4">
          						Loading Report
        					</Typography>
        					<Table className="table">
						        <TableHead>
						          	<TableRow>
						            	<TableCell align="right"><span className="cellBox">Siding Name</span></TableCell>
						            	<TableCell align="right"><span className="cellBox">Placement Date/Time</span></TableCell>
						            	<TableCell align="right"><span className="cellBox">Completion Date/Time</span></TableCell>
						          	</TableRow>
						        </TableHead>
						        <TableBody>

						        	{
						        		loadingData ? 
							        		loadingData.map((item, index)=>{
		                                        return(
		                                        	<TableRow key={item.id}>
										              	<TableCell align="right"><Link to={{pathname:'/loadingData', state: {loadingId: item.id}}} ><span className="cellBox">{item.sidingName}</span></Link></TableCell>
										              	<TableCell align="right"><span className="cellBox">{item.placementDate + '/' + item.placementTime}</span></TableCell>
										              	<TableCell align="right"><span className="cellBox">{item.completionDate + '/' + item.completionTime}</span></TableCell>
										            </TableRow>
		                                        )
		                                    })
		                                : 'No data found'
						        	}
						          	
						        </TableBody>
						     </Table>
      					</CardContent>
    				</Card>
    				<br />
    				<Card className="card">
      					<CardContent>
        					<Typography variant="h5" component="h4">
          						Unloading Report
        					</Typography>
        					<Table className="table">
						        <TableHead>
						          	<TableRow>
						            	<TableCell align="right"><span className="cellBox">PH Name</span></TableCell>
						            	<TableCell align="right"><span className="cellBox">Siding Quantity</span></TableCell>
						            	<TableCell align="right"><span className="cellBox">In - Time</span></TableCell>
						            	<TableCell align="right"><span className="cellBox">Date</span></TableCell>
						            	<TableCell align="right"><span className="cellBox">Recieved Quantity</span></TableCell>
						          	</TableRow>
						        </TableHead>
						        <TableBody>

						        	{
						        		unloadingData ? 
							        		unloadingData.map((item, index)=>{
		                                        return(
		                                        	<TableRow key={item.id}>
										              	<TableCell align="right"><Link to={{pathname:'/unLoadingData', state: {unloadingId: item.id}}} ><span className="cellBox">{item.phName}</span></Link></TableCell>
										              	<TableCell align="right"><span className="cellBox">{item.sidingQuantity}</span></TableCell>
										              	<TableCell align="right"><span className="cellBox">{item.inTime}</span></TableCell>
										              	<TableCell align="right"><span className="cellBox">{item.outTime}</span></TableCell>
										              	<TableCell align="right"><span className="cellBox">{item.receivedQuantity}</span></TableCell>
										            </TableRow>
		                                        )
		                                    })
		                                : 'No data found'
						        	}

						          	
						        </TableBody>
						     </Table>
      					</CardContent>
    				</Card>
    			</div>
                <div className="clearfix" />
            </div>
        );
    }
})

export default Dashboard;
