import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import CircularProgress from 'material-ui/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {register} from "../../API/register";
import './style.css';

const userPerm = JSON.parse(sessionStorage.getItem('userDetails')).accessPermission

const areas = [
  	{
    	value: '1',
    	label: 'Area 1',
  	},
  	{
    	value: '2',
    	label: 'Area 2',
  	},
  	{
    	value: '3',
    	label: 'Area 3',
  	},
  	{
    	value: '4',
    	label: 'Area 4',
  	},
];

const permissionsAll = [
    {
        value: '1',
        label: 'Owner/Director',
    },
    {
        value: '2',
        label: 'Manager Operations',
    },
    {
        value: '3',
        label: 'Field in-charge',
    },
    {
        value: '4',
        label: 'Loading Supervisor',
    },
]

const permissions1 = [
    {
        value: '3',
        label: 'Field in-charge',
    },
    {
        value: '4',
        label: 'Loading Supervisor',
    },
]

const permissions2 = [
    {
        value: '4',
        label: 'Loading Supervisor',
    }
]

class SignUp extends React.Component {

	constructor(props){
        super(props);
        this.state = {
        	loading: false,
            permissions: [],
            respMsg: '',
            snackbar: false,
            vertical: 'top',
            horizontal: 'center',
            inputFontSize: 30,
			labelFontSize: 20,
        	formData: {
        		name: '',
        		email: '',
        		areaOfDuty: '1',
        		sidingName: '1',
        		accessPermission: '4',
        		password: ''
        	},
        	error: {
        		name: false,
        		email: false,
        		areaOfDuty: false,
        		sidingName: false,
        		accessPermission: false,
        		password: false
        	}
        }
    }

    componentDidMount(){
        if (userPerm === '1') {
            this.setState({permissions: permissionsAll})
        } else if (userPerm === '2') {
            this.setState({permissions: permissions1})
        } else {
            this.setState({permissions: permissions2})
        }
    }

    handleChange = name => event => {
    	let formData = Object.assign({}, this.state.formData);
		formData[name] = event.target.value;
		this.setState({formData});
	}

    handleClose = () => {
        this.setState({snackbar: false})
    }

	validateForm = () => {
		let errorList = this.state.error
		const formData = this.state.formData
		let readyToSubmit = true
		Object.entries(errorList).forEach(([key, value]) => {
		    if (formData[key] === '') {
		    	errorList[key] = true
		    	readyToSubmit = false
		    } else {
		    	errorList[key] = false
		    }
		});
		this.setState({error: errorList})
		if (readyToSubmit === true) {
			this.handleSubmit()
		}
	}

	handleSubmit = () => {
		this.setState({loading: true})
		let data = this.state.formData
        data.app = 'connect'
        register(this.registerCallback, data)
	}

    registerCallback = (response) => {
        if (response) {
            this.setState({
                loading: false,
                snackbar: true,
                respMsg: response.msg
            })
        }
    }

    render() {
    	const { formData, error, loading, snackbar, vertical, horizontal, respMsg, permissions, inputFontSize, labelFontSize } = this.state;
        return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">Sign Up</div>
                </div>
                <div className="container">
  					<div className="clearfix" />
                    {
                        (userPerm === '4') ? 'You are not allowed to create accounts' :
                        <Card className="card">
                            <CardContent>
                                <Typography variant="h5" component="h4">
                                    Sign Up for the roles below you
                                </Typography>
                                <TextField
                                    error={ error.name ? 'true' : ''}
                                    label="Name"
                                    className="textField"
                                    required
                                    margin="normal"
                                    inputProps={{
                                        style: {fontSize: inputFontSize} 
                                    }}
                                    InputLabelProps={{style: {fontSize: labelFontSize}}}
                                    onChange={(e) => {
                                        formData.name = e.target.value;
                                    }}
                                />
                                <TextField
                                    error={ error.email ? 'true' : ''}
                                    label="Email"
                                    className="textField"
                                    required
                                    margin="normal"
                                    inputProps={{
                                        style: {fontSize: inputFontSize} 
                                    }}
                                    InputLabelProps={{style: {fontSize: labelFontSize}}}
                                    onChange={(e) => {
                                        formData.email = e.target.value;
                                    }}
                                />
                                <br /> <br />
                                <InputLabel style={{fontSize: labelFontSize}} className="textField" htmlFor="boxType-simple">Area of Duty</InputLabel>
                                <Select
                                    value={formData.areaOfDuty}
                                    error={ error.areaOfDuty ? 'true' : ''}
                                    onChange={this.handleChange('areaOfDuty')}
                                    className="textField"
                                    required
                                    margin="normal"
                                    style={{fontSize: inputFontSize}}
                                    inputProps={{
                                      name: 'boxType',
                                      id: 'boxType-simple',
                                    }}
                                >
                                    {
                                        areas.map((item, index)=>{
                                            return(
                                                <MenuItem key={item.value} value={item.value}>
                                                    {item.label}
                                                </MenuItem>
                                            )
                                        })
                                    }
                                </Select>
                                <br /> <br />
                                <InputLabel style={{fontSize: labelFontSize}} className="textField" htmlFor="boxType-simple">Siding Name</InputLabel>
                                <Select
                                    value={formData.sidingName}
                                    error={ error.sidingName ? 'true' : ''}
                                    onChange={this.handleChange('sidingName')}
                                    className="textField"
                                    required
                                    margin="normal"
                                    style={{fontSize: inputFontSize}}
                                    inputProps={{
                                      name: 'boxType',
                                      id: 'boxType-simple',
                                    }}
                                >
                                    {
                                        areas.map((item, index)=>{
                                            return(
                                                <MenuItem key={item.value} value={item.value}>
                                                    {item.label}
                                                </MenuItem>
                                            )
                                        })
                                    }
                                </Select>
                                <br /> <br />
                                <InputLabel style={{fontSize: labelFontSize}} className="textField" htmlFor="boxType-simple">Access Permission</InputLabel>
                                <Select
                                    value={formData.accessPermission}
                                    error={ error.accessPermission ? 'true' : ''}
                                    onChange={this.handleChange('accessPermission')}
                                    className="textField"
                                    required
                                    margin="normal"
                                    style={{fontSize: inputFontSize}}
                                    inputProps={{
                                      name: 'boxType',
                                      id: 'boxType-simple',
                                    }}
                                >
                                    {
                                        permissions.map((item, index)=>{
                                            return(
                                                <MenuItem key={item.value} value={item.value}>
                                                    {item.label}
                                                </MenuItem>
                                            )
                                        })
                                    }
                                </Select>
                                <br /> <br />
                                <TextField
                                    error={ error.password ? 'true' : ''}
                                    label="Password"
                                    type="password"
                                    className="textField"
                                    required
                                    margin="normal"
                                    inputProps={{
                                        style: {fontSize: inputFontSize} 
                                    }}
                                    InputLabelProps={{style: {fontSize: labelFontSize}}}
                                    onChange={(e) => {
                                        formData.password = e.target.value;
                                    }}
                                />
                                <hr />
                                {
                                    loading ? <CircularProgress size={10} thickness={2} /> :
                                    <Button variant="contained" onClick={this.validateForm} color="primary" className="textField">
                                        Submit
                                     </Button>
                                }
                            </CardContent>
                        </Card>
                    }
    				
    			</div>
                <div className="clearfix" />
                <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={snackbar}
                    autoHideDuration={4000}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{respMsg}</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                      ]}
                    />
            </div>
        );
    }
}

export default SignUp;
