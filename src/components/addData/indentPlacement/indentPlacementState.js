import {observable, decorate, action} from 'mobx'


class IndentPlacementState {

	constructor(){
		this.destinationCode = '';
		this.consigneeCode = '';
		this.division = '';
		this.stationFrom = '';
		this.date = '2017-05-24';
		this.time = '07:30';
		this.indentUnit = 0;
		this.notes = '';
	}

    initialiseData(){
        this.destinationCode = '';
		this.consigneeCode = '';
		this.division = '';
		this.stationFrom = '';
		this.date = '2017-05-24';
		this.time = '07:30';
		this.indentUnit = 0;
		this.notes = '';
    }
}

decorate(IndentPlacementState, {
	destinationCode: observable,
	consigneeCode: observable,
	division: observable,
	stationFrom: observable,
	date: observable,
	time: observable,
	indentUnit: observable,
	notes: observable,

	initialiseData: action,
})

export default new IndentPlacementState();

