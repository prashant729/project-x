import React from 'react';
import { observer } from 'mobx-react';
import IndentPlacementState from "./indentPlacementState";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import CircularProgress from 'material-ui/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import {placementCall} from "../../../API/addDataCall";
import './style.css';


const IndentPlacement = observer(class IndentPlacement extends React.Component {

	constructor(props){
        super(props);
        this.state = {
            loading: false,
            preview: false,
            respMsg: '',
            snackbar: false,
            vertical: 'top',
			horizontal: 'center',
			inputFontSize: 30,
			labelFontSize: 20,
        	error: {
        		destinationCode: false,
				consigneeCode: false,
				division: false,
				stationFrom: false,
				date: false,
				time: false,
				indentUnit: false
        	}
        }
    }

    componentDidMount(){
    	IndentPlacementState.initialiseData()
    }

    handleClose = () => {
        this.setState({snackbar: false})
    }

    validateForm = () => {
		let errorList = this.state.error
		let readyToSubmit = true
		Object.entries(errorList).forEach(([key, value]) => {
		    if (IndentPlacementState[key] === '' || IndentPlacementState[key] === 0) {
		    	errorList[key] = true
		    	readyToSubmit = false
		    } else {
		    	errorList[key] = false
		    }
		});
		this.setState({error: errorList})
		if (readyToSubmit === true) {
			this.setState({ preview: true })
			this.handleSubmit()
		}
	}

	handleSubmit = () => {
		let userId = JSON.parse(sessionStorage.getItem('userDetails')).userId
		const data = {
			destinationCode: IndentPlacementState.destinationCode,
			consigneeCode: IndentPlacementState.consigneeCode,
			division: IndentPlacementState.division,
			stationFrom: IndentPlacementState.stationFrom,
			date: IndentPlacementState.date,
			time: IndentPlacementState.time,
			indentUnit: IndentPlacementState.indentUnit,
			notes: IndentPlacementState.notes,
			uploader: String(userId)
		}
		
		placementCall(this.placementCallback, data)
	}

	placementCallback = (response) => {
		if (response) {
			this.setState({
                loading: false,
                snackbar: true,
                respMsg: response.msg
			})
			IndentPlacementState.initialiseData()
		}
	}

	setPreview = (value) => {
		this.setState({
			preview: value
		})
	}

	checkButtons = () => {
		if (this.state.preview) {
			return (
				<div>
					<Button variant="contained" color="primary" onClick={()=>{this.setPreview(false)}} className="textField">
						Edit
					</Button>
					{
						this.state.loading ? <CircularProgress size={10} thickness={2} /> : 
						<Button variant="contained" color="primary" onClick={this.validateForm} className="textField">
							Confirm
						</Button>
					}
				</div>
			)
		} else {
			return (<Button variant="contained" color="primary" onClick={()=>{this.setPreview(true)}} className="textField">
					        Submit
					    </Button>)
		}				    	
	}

    render() {
    	const {error, loading, snackbar, vertical, horizontal, respMsg, preview, inputFontSize, labelFontSize} = this.state
        return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">Indent Placement</div>
                </div>
                <div className="container">
  					<div className="clearfix" />
    				<Card className="card">
      					<CardContent>
        					
        					<TextField
					          	label="Destination Code"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={preview}
					          	error={ error.destinationCode ? 'true' : ''}
					          	value={IndentPlacementState.destinationCode}
                                onChange={(e) => {
                                    IndentPlacementState.destinationCode = e.target.value;
                                }}
					        />
					        <TextField
					          	label="Consignee Code"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={preview}
					          	error={ error.consigneeCode ? 'true' : ''}
					          	value={IndentPlacementState.consigneeCode}
                                onChange={(e) => {
                                    IndentPlacementState.consigneeCode = e.target.value;
                                }}
					        />
          					<TextField
					          	label="Division"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={preview}
					          	error={ error.division ? 'true' : ''}
					          	value={IndentPlacementState.division}
                                onChange={(e) => {
                                    IndentPlacementState.division = e.target.value;
                                }}
					        />
						    <TextField
					          	label="Station From"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={preview}
					          	error={ error.stationFrom ? 'true' : ''}
					          	value={IndentPlacementState.stationFrom}
                                onChange={(e) => {
                                    IndentPlacementState.stationFrom = e.target.value;
                                }}
					        />
					        <br /> <br />
					        <TextField
						        id="date"
						        label="Date"
								type="date"
								disabled={preview}
						        defaultValue={IndentPlacementState.date}
					          	value={IndentPlacementState.date}
                                onChange={(e) => {
                                    IndentPlacementState.date = e.target.value;
                                }}
								className="textField"
								inputProps={{
									style: {fontSize: inputFontSize, lineHeight: 1} 
								}}
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						    />
						    <br /><br />
						    <TextField
						        id="time"
						        label="Time"
								type="time"
								disabled={preview}
						        defaultValue={IndentPlacementState.time}
					          	value={IndentPlacementState.time}
                                onChange={(e) => {
                                    IndentPlacementState.time = e.target.value;
                                }}
						        className="textField"
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						        inputProps={{
								  step: 300, // 5 min
								  style: {fontSize: inputFontSize, lineHeight: 1} 
						        }}
						    />
						    <br /><br />
						    <TextField
					          	label="Indent Unit"
					          	type="number"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={preview}
					          	error={ error.indentUnit ? 'true' : ''}
					          	value={IndentPlacementState.indentUnit}
                                onChange={(e) => {
                                    IndentPlacementState.indentUnit = e.target.value;
                                }}
					        />
						    <TextField
					          	label="Notes/Remarks"
					          	className="textField"
								margin="normal"
								disabled={preview}
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	value={IndentPlacementState.notes}
                                onChange={(e) => {
                                    IndentPlacementState.notes = e.target.value;
                                }}
					        />
					        <hr />
						    {
						    	this.checkButtons()
						    }
      					</CardContent>
    				</Card>
    			</div>
                <div className="clearfix" />
                <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={snackbar}
                    autoHideDuration={4000}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{respMsg}</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                      ]}
                    />
            </div>
        );
    }
})

export default IndentPlacement;
