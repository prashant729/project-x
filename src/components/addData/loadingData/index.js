import React from 'react';
import { observer } from 'mobx-react';
import moment from 'moment';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import LoadingState from "./loadingState";
import CircularProgress from 'material-ui/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {imageUpload} from "../../../utils";
import {loadingCall, loadingDataById} from "../../../API/addDataCall";
import './style.css';

const {sessionStorage} = window;


const boxTypes = [
  	{
    	value: 'N',
    	label: 'N',
  	},
  	{
    	value: 'NHL',
    	label: 'NHL',
  	},
  	{
    	value: 'NR',
    	label: 'NR',
  	},
  	{
    	value: 'NHA',
    	label: 'NHA',
  	},
];

const issueTypes = [
	{
		value: '1',
		label: 'Door',
	},
	{
		value: '2',
		label: 'Floor',
	},
	{
		value: '3',
		label: 'Side Panel',
	},
];

const coalGrades = [
	{
		value: 'G1',
		label: 'G1',
	},
	{
		value: 'G2',
		label: 'G2',
	},
	{
		value: 'G3',
		label: 'G3',
	},
	{
		value: 'G4',
		label: 'G4',
	},
	{
		value: 'G5',
		label: 'G5',
	},
	{
		value: 'G6',
		label: 'G6',
	},
	{
		value: 'G7',
		label: 'G7',
	},
	{
		value: 'G8',
		label: 'G8',
	},
	{
		value: 'G9',
		label: 'G9',
	},
	{
		value: 'G10',
		label: 'G10',
	},
	{
		value: 'G11',
		label: 'G11',
	},
	{
		value: 'G12',
		label: 'G12',
	},
	{
		value: 'G13',
		label: 'G13',
	},
	{
		value: 'G14',
		label: 'G14',
	},
	{
		value: 'G15',
		label: 'G15',
	},
]

const issues = [
	{
		value: 'yes',
		label: 'Yes',
	},
	{
		value: 'no',
		label: 'No',
	}
];


const LoadingData = observer(class LoadingData extends React.Component {

	constructor(props){
        super(props);
        this.state = {
        	loading: false,
            respMsg: '',
            snackbar: false,
            vertical: 'top',
            horizontal: 'center',
			preview: false,
			edit: false,
			inputFontSize: 30,
			labelFontSize: 20,
        	error: {
        		sidingName: false,
        		boxType: false,
				numberOfBoxes: false,
				issue: false,
				firstBoxNumber: false,
				lastBoxNumber: false,
				coalGrade: false,
				rakeNumber: false,
				destination: false,
				grossWeight: false,
				tareWeight: false,
				netWeight: false,
				numberOfLoadedBoxes: false,
				rrNumber: false,
				invoiceNumber: false,
				placementDate: false,
				placementTime: false,
				dateOfCompletion: false,
				timeOfCompletion: false,
				rrDate: false,
				invoiceDate: false
        	}
        }
    }

    componentDidMount(){
		if (this.props.location.state) {
			this.setState({
				edit: true
			})
			loadingDataById(this.loadingDataCallback, this.props.location.state.loadingId)
		} else {
			LoadingState.initialiseData()
		}
	}
	
	loadingDataCallback = (response) => {
		this.fillLoadingData(response.response)
	}

	fillLoadingData = (values) => {
		Object.entries(LoadingState).forEach(([key, value]) => {
		    LoadingState[key] = values[key]
		});
	}

    handleChange = name => event => {
    	LoadingState[name] = event.target.value
  	}

  	handleClose = () => {
        this.setState({snackbar: false})
    }

  	imageUpload = name => e => {
	    imageUpload(e).then(res => {
	    	LoadingState[name] = res.encoded_image
	    });
	};

	validateForm = () => {
		let errorList = this.state.error
		let readyToSubmit = true
		Object.entries(errorList).forEach(([key, value]) => {
		    if (LoadingState[key] === '' || LoadingState[key] === 0) {
		    	errorList[key] = true
		    	readyToSubmit = false
		    } else {
		    	errorList[key] = false
		    }
		});
		this.setState({error: errorList})
		if (readyToSubmit === true) {
			this.setState({ preview: true })
			this.handleSubmit()
		} else {
			this.setState({ preview: true })
			this.handleSubmit()
		}
	}

	handleSubmit = () => {
		let userId = JSON.parse(sessionStorage.getItem('userDetails')).userId
		const data = {
			sidingName: LoadingState.sidingName,
			boxType: LoadingState.boxType,
			numberOfBoxes: LoadingState.numberOfBoxes,
			issueWithBox: LoadingState.issue,
			wagonNumber: LoadingState.wagonNumber,
			issueType: LoadingState.issueType,
			firstBoxNumber: LoadingState.firstBoxNumber,
			lastBoxNumber: LoadingState.lastBoxNumber,
			coalGrade: LoadingState.coalGrade,
			rakeNumber: LoadingState.rakeNumber,
			destination: LoadingState.destination,
			grossWeight: LoadingState.grossWeight,
			tareWeight: LoadingState.tareWeight,
			netWeight: LoadingState.netWeight,
			numberOfLoadedBoxes: LoadingState.numberOfLoadedBoxes,
			rrNumber: LoadingState.rrNumber,
			invoiceNumber: LoadingState.invoiceNumber,
			notes: LoadingState.notes,
			issueImage: LoadingState.issueImage,
			rrImage: LoadingState.rrImage,
			placementDate: LoadingState.placementDate,
			placementTime: LoadingState.placementTime,
			dateOfCompletion: LoadingState.dateOfCompletion,
			timeOfCompletion: LoadingState.timeOfCompletion,
			departureFinalDate: LoadingState.departureFinalDate,
			departureFinalTime: LoadingState.departureFinalTime,
			rrDate: LoadingState.rrDate,
			invoiceDate: LoadingState.invoiceDate,
			uploader: String(userId)
		}
		
		loadingCall(this.loadingCallback, data)
	}

	loadingCallback = (response) => {
		if (response) {
			this.setState({
                loading: false,
                snackbar: true,
                respMsg: response.msg
            })
            this.props.history.push('/dashboard')
		}
	}

	setPreview = (value) => {
		this.setState({
			preview: value
		})
	}

	checkButtons = () => {
		if (this.state.preview) {
			return (
				<div>
					<Button variant="contained" color="primary" onClick={()=>{this.setPreview(false)}} className="textField">
						Edit
					</Button>
					{
						this.state.loading ? <CircularProgress size={10} thickness={2} /> : 
						<Button variant="contained" color="primary" onClick={this.validateForm} className="textField">
							Confirm
						</Button>
					}
				</div>
			)
		} else {
			return (<Button disabled={this.state.edit} variant="contained" color="primary" onClick={()=>{this.setPreview(true)}} className="textField">
					        Submit
					    </Button>)
		}				    	
	}

    render() {
    	const {error, loading, snackbar, vertical, horizontal, respMsg, preview, inputFontSize, labelFontSize} = this.state
        return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">Loading Data</div>
                </div>
                <div className="container">
  					<div className="clearfix" />
    				<Card className="card">
      					<CardContent>
        					
        					<TextField
					          	error={ error.sidingName ? 'true' : ''}
					          	label="Siding Name"
					          	className="textField"
					          	margin="normal"
								required
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	disabled={ preview }
					          	value={LoadingState.sidingName}
                                onChange={(e) => {
                                    LoadingState.sidingName = e.target.value;
                                }}
					        />
					        <br /> <br />
					        <InputLabel style={{fontSize: labelFontSize}} className="textField" htmlFor="boxType-simple">Box Type</InputLabel>
					        <Select
					            value={LoadingState.boxType}
					            error={ error.boxType ? 'true' : ''}
					            onChange={this.handleChange('boxType')}
					            className="textField"
					            margin="normal"
								required
								style={{fontSize: inputFontSize}}
					            disabled={ preview }
					            inputProps={{
					              name: 'boxType',
								  id: 'boxType-simple'
					            }}
          					>
	            				{
					        		boxTypes.map((item, index)=>{
                                        return(
                                            <MenuItem key={item.value} value={item.value}>
                                            	{item.label}
                                            </MenuItem>
                                        )
                                    })
					        	}
          					</Select>
          					<br /> <br />
					        <TextField
					          	label="Number of boxes"
					          	error={ error.numberOfBoxes ? 'true' : ''}
					          	type="number"
					          	className="textField"
								  margin="normal"
								  inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	value={LoadingState.numberOfBoxes}
                                onChange={(e) => {
                                    LoadingState.numberOfBoxes = e.target.value;
                                }}
					        />
					        <br /> <br />
					        <InputLabel style={{fontSize: labelFontSize}} className="textField" htmlFor="issues-simple">Issues with the box</InputLabel>
					        <Select
					            value={LoadingState.issue}
					            error={ error.issue ? 'true' : ''}
					            onChange={this.handleChange('issue')}
					            className="textField"
								margin="normal"
								style={{fontSize: inputFontSize}}
					            required
					            disabled={ preview }
					            inputProps={{
					              name: 'issue',
					              id: 'issues-simple',
					            }}
          					>
	            				{
					        		issues.map((item, index)=>{
                                        return(
                                            <MenuItem key={item.value} value={item.value}>
                                            	{item.label}
                                            </MenuItem>
                                        )
                                    })
					        	}
          					</Select>
          					<br /> <br />
          					<TextField
					          	label="Wagon Number"
					          	className="textField"
								  margin="normal"
								  inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	type="number"
					          	required
					          	disabled={ preview }
					          	value={LoadingState.wagonNumber}
                                onChange={(e) => {
                                    LoadingState.wagonNumber = e.target.value;
                                }}
					        />
					        <br /> <br />
					        <InputLabel style={{fontSize: labelFontSize}} className="textField" htmlFor="issueType-simple">Issue Type</InputLabel>
					        <Select
					            value={LoadingState.issueType}
					            onChange={this.handleChange('issueType')}
					            className="textField"
								margin="normal"
								style={{fontSize: inputFontSize}}
					            required
					            disabled={ preview }
					            inputProps={{
					              name: 'issueType',
								  id: 'issueType-simple'
					            }}
          					>
	            				{
					        		issueTypes.map((item, index)=>{
                                        return(
                                            <MenuItem key={item.value} value={item.value}>
                                            	{item.label}
                                            </MenuItem>
                                        )
                                    })
					        	}
          					</Select>
          					<br /><br />
          					{
								(this.state.edit && LoadingState.issueImage) ? <img src={LoadingState.issueImage} alt='issue image' width='290px' />
								  : 
								<div><input
									accept="image/*"
									style={{'display': 'none'}}
									id="contained-button-file"
									type="file"
									onChange={this.imageUpload('issueImage')}
								/>
								<label htmlFor="contained-button-file">
									<Button variant="contained" color="primary" component="span">
											Upload Image of Issue
									</Button>
								</label></div>
							}
						    <TextField
					          	error={ error.firstBoxNumber ? 'true' : ''}
					          	label="First Box Number"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	type="number"
					          	disabled={ preview }
					          	value={LoadingState.firstBoxNumber}
                                onChange={(e) => {
                                    LoadingState.firstBoxNumber = e.target.value;
                                }}
					        />
					        <TextField
					          	error={ error.lastBoxNumber ? 'true' : ''}
					          	label="Last Box Number"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	type="number"
					          	disabled={ preview }
					          	value={LoadingState.lastBoxNumber}
                                onChange={(e) => {
                                    LoadingState.lastBoxNumber = e.target.value;
                                }}
					        />
					        <br /> <br />
					        <InputLabel style={{fontSize: labelFontSize}} className="textField" htmlFor="coalGrade-simple">Coal Grade</InputLabel>
					        <Select
					            value={LoadingState.coalGrade}
					            error={ error.coalGrade ? 'true' : ''}
					            onChange={this.handleChange('coalGrade')}
					            className="textField"
								margin="normal"
								style={{fontSize: inputFontSize}}
					            required
					            disabled={ preview }
					            inputProps={{
					              name: 'coalGrade',
					              id: 'coalGrade-simple',
					            }}
          					>
	            				{
					        		coalGrades.map((item, index)=>{
                                        return(
                                            <MenuItem key={item.value} value={item.value}>
                                            	{item.label}
                                            </MenuItem>
                                        )
                                    })
					        	}
          					</Select>
          					<br /> <br />
          					<TextField
					          	error={ error.rakeNumber ? 'true' : ''}
					          	label="Rake Number"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	type="number"
					          	value={LoadingState.rakeNumber}
                                onChange={(e) => {
                                    LoadingState.rakeNumber = e.target.value;
                                }}
					        />
					        <TextField
					          	error={ error.destination ? 'true' : ''}
					          	label="Destination"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	value={LoadingState.destination}
                                onChange={(e) => {
                                    LoadingState.destination = e.target.value;
                                }}
					        />
					        <br /><br />
					        <TextField
						        id="date"
						        label="Placement Date"
								type="date"
								inputProps={{
									style: {fontSize: inputFontSize, lineHeight: 1} 
								}}
								disabled={ preview }
						        defaultValue={LoadingState.placementDate}
						        value={LoadingState.placementDate}
						        onChange={(e) => {
                                    LoadingState.placementDate = e.target.value;
                                }}
						        className="textField"
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						    />
						    <br /><br />
						    <TextField
						        id="time"
						        label="Placement Time"
								type="time"
								disabled={ preview }
						        defaultValue={LoadingState.placementTime}
						        value={moment(LoadingState.placementTime, 'HH:mm').format('HH:mm')}
						        onChange={(e) => {
                                    LoadingState.placementTime = e.target.value;
                                }}
								className="textField"
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						        inputProps={{
								  step: 300, // 5 min
								  style: {fontSize: inputFontSize, lineHeight: 1} 
						        }}
						    />
						    <br /><br />
						    <TextField
						        id="date"
						        label="Date of Completion"
								type="date"
								disabled={ preview }
						        defaultValue={LoadingState.dateOfCompletion}
						        value={LoadingState.dateOfCompletion}
						        onChange={(e) => {
                                    LoadingState.dateOfCompletion = e.target.value;
								}}
								inputProps={{
									style: {fontSize: inputFontSize, lineHeight: 1} 
								}}
						        className="textField"
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						    />
						    <br /><br />
						    <TextField
						        id="time"
						        label="Time of Completion"
								type="time"
								disabled={ preview }
						        defaultValue={LoadingState.timeOfCompletion}
						        value={moment(LoadingState.timeOfCompletion, 'HH:mm').format('HH:mm')}
						        onChange={(e) => {
                                    LoadingState.timeOfCompletion = e.target.value;
                                }}
						        className="textField"
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						        inputProps={{
								  step: 300, // 5 min
								  style: {fontSize: inputFontSize, lineHeight: 1} 
						        }}
						    />
						    <br /><br />
						    <TextField
						        id="date"
						        label="Departure Final Date"
								type="date"
								disabled={ preview }
						        defaultValue={LoadingState.departureFinalDate}
						        value={LoadingState.departureFinalDate}
						        onChange={(e) => {
                                    LoadingState.departureFinalDate = e.target.value;
                                }}
								className="textField"
								inputProps={{
									style: {fontSize: inputFontSize, lineHeight: 1} 
								}}
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						    />
						    <br /><br />
						    <TextField
						        id="time"
						        label="Departure Final Time"
								type="time"
								disabled={ preview }
						        defaultValue={LoadingState.departureFinalTime}
						        value={moment(LoadingState.departureFinalTime, 'HH:mm').format('HH:mm')}
						        onChange={(e) => {
                                    LoadingState.departureFinalTime = e.target.value;
                                }}
								className="textField"
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						        inputProps={{
								  step: 300, // 5 min
								  style: {fontSize: inputFontSize, lineHeight: 1} 
						        }}
						    />
						    <br /> <br />
						    <TextField
					          	error={ error.grossWeight ? 'true' : ''}
					          	label="Gross Weight"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	type="number"
					          	value={LoadingState.grossWeight}
                                onChange={(e) => {
                                    LoadingState.grossWeight = e.target.value;
                                }}
					        />
					        <TextField
					          	error={ error.tareWeight ? 'true' : ''}
					          	label="Tare Weight"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	type="number"
					          	value={LoadingState.tareWeight}
                                onChange={(e) => {
                                    LoadingState.tareWeight = e.target.value;
                                }}
					        />
					        <TextField
					          	error={ error.netWeight ? 'true' : ''}
					          	label="Net Weight"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	type="number"
					          	value={LoadingState.netWeight}
                                onChange={(e) => {
                                    LoadingState.netWeight = e.target.value;
                                }}
					        />
					        <TextField
					          	label="Number of Loaded Boxes"
					          	error={ error.numberOfLoadedBoxes ? 'true' : ''}
					          	type="number"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	value={LoadingState.numberOfLoadedBoxes}
                                onChange={(e) => {
                                    LoadingState.numberOfLoadedBoxes = e.target.value;
                                }}
					        />
					        <TextField
					          	error={ error.rrNumber ? 'true' : ''}
					          	label="RR Number"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	value={LoadingState.rrNumber}
                                onChange={(e) => {
                                    LoadingState.rrNumber = e.target.value;
                                }}
					        />
					        <br /><br />
						    <TextField
						        id="date"
						        label="RR Date"
								type="date"
								disabled={ preview }
						        defaultValue={LoadingState.rrDate}
						        value={LoadingState.rrDate}
						        onChange={(e) => {
                                    LoadingState.rrDate = e.target.value;
                                }}
								className="textField"
								inputProps={{
									style: {fontSize: inputFontSize, lineHeight: 1} 
								}}
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						    />
						    <br /><br />
						    <TextField
					          	error={ error.invoiceNumber ? 'true' : ''}
					          	label="Invoice Number"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	required
					          	disabled={ preview }
					          	type="number"
					          	value={LoadingState.invoiceNumber}
                                onChange={(e) => {
                                    LoadingState.invoiceNumber = e.target.value;
                                }}
					        />
					        <br /><br />
						    <TextField
						        id="date"
						        label="Invoice Date"
								type="date"
								disabled={ preview }
						        defaultValue={LoadingState.invoiceDate}
						        value={LoadingState.invoiceDate}
						        onChange={(e) => {
                                    LoadingState.invoiceDate = e.target.value;
                                }}
								className="textField"
								inputProps={{
									style: {fontSize: inputFontSize, lineHeight: 1} 
								}}
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						    />
						    <br /><br />
						    <TextField
					          	label="Notes/Remarks"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	disabled={ preview }
					          	value={LoadingState.notes}
                                onChange={(e) => {
                                    LoadingState.notes = e.target.value;
                                }}
					        />
					        <br /><br />
          					{
								(this.state.edit && LoadingState.rrImage) ? <img src={LoadingState.rrImage} width='290px' alt='rr image' />
								:
								<div>
									<input
										accept="image/*"
										style={{'display': 'none'}}
										id="rr-button-file"
										type="file"
										onChange={this.imageUpload('rrImage')}
									/>
									<label htmlFor="rr-button-file">
										<Button variant="contained" color="primary" component="span">
											Upload RR Image
										</Button>
									</label>
								</div>
							}
						    <hr />
						    {
						    	this.checkButtons()
						    }
						    
      					</CardContent>
    				</Card>
    			</div>
                <div className="clearfix" />
                <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={snackbar}
                    autoHideDuration={4000}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{respMsg}</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                      ]}
                    />
            </div>
        );
    }
})

export default LoadingData;
