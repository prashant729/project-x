import {observable, decorate, action} from 'mobx'


class LoadingState {

	constructor(){
		this.sidingName = '';
		this.boxType = 'N';
		this.numberOfBoxes = 0;
		this.issue = 'yes';
		this.wagonNumber = 0;
		this.issueType = '1';
		this.firstBoxNumber = 0;
		this.lastBoxNumber = 0;
		this.coalGrade = 'G1';
		this.rakeNumber = 0;
		this.destination = '';
		this.grossWeight = 0;
		this.tareWeight = 0;
		this.netWeight = 0;
		this.numberOfLoadedBoxes = 0;
		this.rrNumber = '';
		this.invoiceNumber = 0;
		this.notes = '';
		this.issueImage = '';
		this.rrImage = '';
		this.placementDate = "2017-05-24";
		this.placementTime = "07:30";
		this.dateOfCompletion = "2017-05-24";
		this.timeOfCompletion = "07:30";
		this.departureFinalDate = "2017-05-24";
		this.departureFinalTime = "07:30";
		this.rrDate = "2017-05-24";
		this.invoiceDate = "2017-05-24";
	}

    initialiseData(){
        this.sidingName = '';
        this.boxType = 'N';
        this.numberOfBoxes = 0;
        this.issue = 'yes';
        this.wagonNumber = 0;
        this.issueType = '1';
        this.firstBoxNumber = 0;
        this.lastBoxNumber = 0;
        this.coalGrade = 'G1';
        this.rakeNumber = 0;
        this.destination = '';
        this.grossWeight = 0;
        this.tareWeight = 0;
        this.netWeight = 0;
		this.numberOfLoadedBoxes = 0;
		this.rrNumber = '';
		this.invoiceNumber = 0;
		this.notes = '';
		this.issueImage = '';
		this.rrImage = '';
		this.placementDate = "2017-05-24";
		this.placementTime = "07:30";
		this.dateOfCompletion = "2017-05-24";
		this.timeOfCompletion = "07:30";
		this.departureFinalDate = "2017-05-24";
		this.departureFinalTime = "07:30";
		this.rrDate = "2017-05-24";
		this.invoiceDate = "2017-05-24";
    }
}

decorate(LoadingState, {
	sidingName: observable,
	boxType: observable,
	numberOfBoxes: observable,
	issue: observable,
	wagonNumber: observable,
	issueType: observable,
	firstBoxNumber: observable,
	lastBoxNumber: observable,
	coalGrade: observable,
	rakeNumber: observable,
	destination: observable,
	grossWeight: observable,
	tareWeight: observable,
	netWeight: observable,
	numberOfLoadedBoxes: observable,
	rrNumber: observable,
	invoiceNumber: observable,
	notes: observable,
	issueImage: observable,
	rrImage: observable,
	placementDate: observable,
	placementTime: observable,
	dateOfCompletion: observable,
	timeOfCompletion: observable,
	departureFinalDate: observable,
	departureFinalTime: observable,
	rrDate: observable,
	invoiceDate: observable,

	initialiseData: action,
})

export default new LoadingState();

