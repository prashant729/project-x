import React from 'react';
import { observer } from 'mobx-react';
import RakePositionState from "./rakePositionState";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {rakePositionCall} from "../../../API/addDataCall";
import './style.css';


const RakePosition = observer(class RakePosition extends React.Component {

	constructor(props){
        super(props);
        this.state = {
            loading: false,
            respMsg: '',
            snackbar: false,
            vertical: 'top',
			horizontal: 'center',
			inputFontSize: 30,
			labelFontSize: 20,
        }
	}
	
	componentDidMount(){
		RakePositionState.initialiseData()
	}

	handleSubmit = () => {
		let userId = JSON.parse(sessionStorage.getItem('userDetails')).userId
		const data = {
			rakeNumber: RakePositionState.rakeNumber,
			area: RakePositionState.area,
			positionOfRake: RakePositionState.positionOfRake,
			expectedArrival: RakePositionState.expectedArrival,
			notes: RakePositionState.notes,
			uploader: String(userId)
		}
		rakePositionCall(this.rakePositionCallback, data)
	}

	rakePositionCallback = (response) => {
		if (response) {
			this.setState({
                loading: false,
                snackbar: true,
                respMsg: response.msg
            })
            RakePositionState.initialiseData()
		}
	}

	handleClose = () => {
        this.setState({snackbar: false})
    }

    render() {
		const {loading, snackbar, vertical, horizontal, respMsg, inputFontSize, labelFontSize} = this.state
        return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">Rake Position</div>
                </div>
                <div className="container">
  					<div className="clearfix" />
    				<Card className="card">
      					<CardContent>
        					
        					<TextField
					          	label="Rake Number"
								className="textField"
								type="number"  
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
								value={RakePositionState.rakeNumber}
								onChange={(e) => {
									RakePositionState.rakeNumber = e.target.value;
								}}
					        />
					        <TextField
					          	label="Area"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
								value={RakePositionState.area}
								onChange={(e) => {
									RakePositionState.area = e.target.value;
								}}  
					        />
          					<TextField
					          	label="Position of Rake"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
								value={RakePositionState.positionOfRake}
								onChange={(e) => {
									RakePositionState.positionOfRake = e.target.value;
								}}
					        />
					        <br /><br />
					        <TextField
						        id="date"
						        label="Expected Arrival"
						        type="date"
						        defaultValue={RakePositionState.expectedArrival}
								className="textField"
								value={RakePositionState.expectedArrival}
								onChange={(e) => {
									RakePositionState.expectedArrival = e.target.value;
								}}
								inputProps={{
									style: {fontSize: inputFontSize, lineHeight: 1} 
								}}
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						    />
						    <br /><br />
						    <TextField
					          	label="Notes/Remarks"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
								value={RakePositionState.notes}
								onChange={(e) => {
									RakePositionState.notes = e.target.value;
								}}
								
					        />
      					</CardContent>
    				</Card>
    				<br/>
    				<br />
    				<Button variant="contained" color="primary" onClick={this.handleSubmit} className="textField">
				        Add Entry
				     </Button>
    			</div>
                <div className="clearfix" />
				<Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={snackbar}
                    autoHideDuration={4000}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{respMsg}</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                      ]}
                    />
            </div>
        );
    }
})

export default RakePosition;
