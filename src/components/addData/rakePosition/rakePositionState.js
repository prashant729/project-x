import {observable, decorate, action} from 'mobx'


class RakePositionState {

	constructor(){
		this.rakeNumber = 0;
		this.area = '';
        this.positionOfRake = '';
        this.expectedArrival = '2017-05-24';
		this.notes = '';
	}

    initialiseData(){
        this.rakeNumber = 0;
		this.area = '';
        this.positionOfRake = '';
        this.expectedArrival = '2017-05-24';
		this.notes = '';
    }
}

decorate(RakePositionState, {
	rakeNumber: observable,
    area: observable,
    positionOfRake: observable,
    expectedArrival: observable,
    notes: observable,

	initialiseData: action,
})

export default new RakePositionState();

