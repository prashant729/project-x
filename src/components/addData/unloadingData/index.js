import React from 'react';
import moment from 'moment';
import { observer } from 'mobx-react';
import UnloadingState from "./unloadingState";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from 'material-ui/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {imageUpload} from "../../../utils";
import {unloadingCall, unloadingDataById} from "../../../API/addDataCall";
import './style.css';


const UnloadingData = observer(class UnloadingData extends React.Component {

	constructor(props){
        super(props);
        this.state = {
            loading: false,
            preview: false,
            respMsg: '',
            snackbar: false,
            vertical: 'top',
			horizontal: 'center',
			edit: false,
			inputFontSize: 30,
			labelFontSize: 20,
        	error: {
        		rrNumber: false,
				powerhouseName: false,
				rakeNumber: false,
				receivedQuantity: false,
				numberOfBoxes: false,
				inTime: false,
				outTime: false,
				date: false,
				sidingQuantity: false,
				tiplerWeight: false,
				grossWeight: false,
				tierWeight: false,
				netWeight: false,
				rrImage: false,
        	}
        }
    }

    componentDidMount(){
    	if (this.props.location.state) {
			this.setState({
				edit: true
			})
			unloadingDataById(this.unloadingDataCallback, this.props.location.state.unloadingId)
		} else {
			UnloadingState.initialiseData()
		}
	}
	
	unloadingDataCallback = (response) => {
		this.fillUnloadingData(response.response)
	}

	fillUnloadingData = (values) => {
		Object.entries(UnloadingState).forEach(([key, value]) => {
		    UnloadingState[key] = values[key]
		});
	}

    handleClose = () => {
        this.setState({snackbar: false})
    }

  	imageUpload = name => e => {
	    imageUpload(e).then(res => {
	    	UnloadingState[name] = res.encoded_image
	    });
	};

	validateForm = () => {
		let errorList = this.state.error
		let readyToSubmit = true
		Object.entries(errorList).forEach(([key, value]) => {
		    if (UnloadingState[key] === '' || UnloadingState[key] === 0) {
		    	errorList[key] = true
		    	readyToSubmit = false
		    } else {
		    	errorList[key] = false
		    }
		});
		this.setState({error: errorList})
		if (readyToSubmit === true) {
			this.setState({ preview: true })
			this.handleSubmit()
		} else {
			this.setState({ preview: true })
			this.handleSubmit()
		}
	}

	handleSubmit = () => {
		let userId = JSON.parse(sessionStorage.getItem('userDetails')).userId
		const data = {
			rrNumber: UnloadingState.rrNumber,
			powerhouseName: UnloadingState.powerhouseName,
			rakeNumber: UnloadingState.rakeNumber,
			receivedQuantity: UnloadingState.receivedQuantity,
			numberOfBoxes: UnloadingState.numberOfBoxes,
			inTime: UnloadingState.inTime,
			outTime: UnloadingState.outTime,
			date: UnloadingState.date,
			sidingQuantity: UnloadingState.sidingQuantity,
			tiplerWeight: UnloadingState.tiplerWeight,
			grossWeight: UnloadingState.grossWeight,
			tierWeight: UnloadingState.tierWeight,
			netWeight: UnloadingState.netWeight,
			notes: UnloadingState.notes,
			rrImage: UnloadingState.rrImage,
			uploader: String(userId)
		}
		
		unloadingCall(this.unloadingCallback, data)
	}

	unloadingCallback = (response) => {
		if (response) {
			this.setState({
                loading: false,
                snackbar: true,
                respMsg: response.msg
			})
			this.props.history.push('/dashboard')
		}
	}

	setPreview = (value) => {
		this.setState({
			preview: value
		})
	}

	checkButtons = () => {
		if (this.state.preview) {
			return (
				<div>
					<Button variant="contained" color="primary" onClick={()=>{this.setPreview(false)}} className="textField">
						Edit
					</Button>
					{
						this.state.loading ? <CircularProgress size={10} thickness={2} /> : 
						<Button disabled={this.state.edit} variant="contained" color="primary" onClick={this.validateForm} className="textField">
							Confirm
						</Button>
					}
				</div>
			)
		} else {
			return (<Button variant="contained" color="primary" onClick={()=>{this.setPreview(true)}} className="textField">
					        Submit
					    </Button>)
		}				    	
	}

    render() {
    	const {error, loading, snackbar, vertical, horizontal, respMsg, preview, inputFontSize, labelFontSize} = this.state
        return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">Unloading Data</div>
                </div>
                <div className="container">
  					<div className="clearfix" />
    				<Card className="card">
      					<CardContent>
        					
        					<TextField
					          	label="RR Number"
					          	className="textField"
					          	required
					          	disabled={preview}
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.rrNumber ? 'true' : ''}
					          	value={UnloadingState.rrNumber}
                                onChange={(e) => {
                                    UnloadingState.rrNumber = e.target.value;
                                }}
					        />
					        <TextField
					          	label="Powerhouse Name"
					          	className="textField"
					          	required
					          	disabled={preview}
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.powerhouseName ? 'true' : ''}
					          	value={UnloadingState.powerhouseName}
                                onChange={(e) => {
                                    UnloadingState.powerhouseName = e.target.value;
                                }}
					        />
          					<TextField
					          	label="Rake Number"
					          	className="textField"
					          	required
					          	disabled={preview}
					          	type="number"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.rakeNumber ? 'true' : ''}
					          	value={UnloadingState.rakeNumber}
                                onChange={(e) => {
                                    UnloadingState.rakeNumber = e.target.value;
                                }}
					        />
						    <TextField
					          	label="Received Quantity"
					          	className="textField"
					          	required
					          	disabled={preview}
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.receivedQuantity ? 'true' : ''}
					          	value={UnloadingState.receivedQuantity}
                                onChange={(e) => {
                                    UnloadingState.receivedQuantity = e.target.value;
                                }}
					        />
					        <TextField
					          	label="Number of Boxes"
					          	type="number"
					          	className="textField"
					          	required
					          	disabled={preview}
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.numberOfBoxes ? 'true' : ''}
					          	value={UnloadingState.numberOfBoxes}
                                onChange={(e) => {
                                    UnloadingState.numberOfBoxes = e.target.value;
                                }}
					        />
					        <br /><br />
						    <TextField
						        id="time"
						        label="In Time"
								type="time"
								disabled={ preview }
						        defaultValue={UnloadingState.inTime}
					          	value={moment(UnloadingState.inTime, 'HH:mm').format('HH:mm')}
                                onChange={(e) => {
                                    UnloadingState.inTime = e.target.value;
                                }}
						        className="textField"
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						        inputProps={{
								  step: 300, // 5 min
								  style: {fontSize: inputFontSize, lineHeight: 1} 
						        }}
						    />
						    <br /><br />
						    <TextField
						        id="time"
						        label="Out Time"
								type="time"
								disabled={ preview }
						        defaultValue={UnloadingState.outTime}
					          	value={moment(UnloadingState.outTime, 'HH:mm').format('HH:mm')}
                                onChange={(e) => {
                                    UnloadingState.outTime = e.target.value;
                                }}
						        className="textField"
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						        inputProps={{
								  step: 300, // 5 min
								  style: {fontSize: inputFontSize, lineHeight: 1} 
						        }}
						    />
          					<br /> <br />
					        <TextField
						        id="date"
						        label="Date"
								type="date"
								disabled={ preview }
						        defaultValue={UnloadingState.date}
								value={UnloadingState.date}
                                onChange={(e) => {
                                    UnloadingState.date = e.target.value;
                                }}
								className="textField"
								inputProps={{
									style: {fontSize: inputFontSize, lineHeight: 1} 
								}}
						        InputLabelProps={{
								  shrink: true,
								  style: {fontSize: labelFontSize}
						        }}
						    />
						    <br /><br />
						    <TextField
					          	label="Siding Quantity"
					          	className="textField"
					          	required
					          	disabled={preview}
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.sidingQuantity ? 'true' : ''}
					          	value={UnloadingState.sidingQuantity}
                                onChange={(e) => {
                                    UnloadingState.sidingQuantity = e.target.value;
                                }}
					        />
					        <TextField
					          	label="Tipler Weight"
					          	className="textField"
					          	required
					          	disabled={preview}
					          	type="number"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.tiplerWeight ? 'true' : ''}
					          	value={UnloadingState.tiplerWeight}
                                onChange={(e) => {
                                    UnloadingState.tiplerWeight = e.target.value;
                                }}
					        />
					        <TextField
					          	label="Gross Weight"
					          	className="textField"
					          	required
					          	disabled={preview}
					          	type="number"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.grossWeight ? 'true' : ''}
					          	value={UnloadingState.grossWeight}
                                onChange={(e) => {
                                    UnloadingState.grossWeight = e.target.value;
                                }}
					        />
					        <TextField
					          	label="Tier Weight"
					          	className="textField"
					          	required
					          	disabled={preview}
					          	type="number"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.tierWeight ? 'true' : ''}
					          	value={UnloadingState.tierWeight}
                                onChange={(e) => {
                                    UnloadingState.tierWeight = e.target.value;
                                }}
					        />
					        <TextField
					          	label="Net Weight"
					          	className="textField"
					          	required
					          	disabled={preview}
					          	type="number"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	error={ error.netWeight ? 'true' : ''}
					          	value={UnloadingState.netWeight}
                                onChange={(e) => {
                                    UnloadingState.netWeight = e.target.value;
                                }}
					        />
						    <TextField
					          	label="Notes/Remarks"
					          	className="textField"
					          	disabled={preview}
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
					          	value={UnloadingState.notes}
                                onChange={(e) => {
                                    UnloadingState.notes = e.target.value;
                                }}
					        />
					        <br /><br />
          					{
								(this.state.edit && UnloadingState.rrImage) ?
								<img src={UnloadingState.rrImage} width='290px' alt='rr image' />
								:
								<div>
									<input
										accept="image/*"
										style={{'display': 'none'}}
										id="rr-button-file"
										type="file"
										onChange={this.imageUpload('rrImage')}
									/>
									<label htmlFor="rr-button-file">
										<Button variant="contained" color="primary" component="span">
											Upload RR Image
										</Button>
									</label>
								</div>
							}
						    <hr />
						    {
						    	this.checkButtons()
						    }
      					</CardContent>
    				</Card>
    			</div>
                <div className="clearfix" />
                <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={snackbar}
                    autoHideDuration={4000}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{respMsg}</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                      ]}
                    />
            </div>
        );
    }
})

export default UnloadingData;