import {observable, decorate, action} from 'mobx'


class UnloadingState {

	constructor(){
		this.rrNumber = '';
		this.powerhouseName = '';
		this.rakeNumber = 0;
		this.receivedQuantity = '';
		this.numberOfBoxes = 0;
		this.inTime = '07:30';
		this.outTime = '07:30';
		this.date = '2017-05-24';
		this.sidingQuantity = '';
		this.tiplerWeight = 0;
		this.grossWeight = 0;
		this.tierWeight = 0;
		this.netWeight = 0;
		this.notes = '';
		this.rrImage = '';
	}

    initialiseData(){
        this.rrNumber = '';
		this.powerhouseName = '';
		this.rakeNumber = 0;
		this.receivedQuantity = '';
		this.numberOfBoxes = 0;
		this.inTime = '07:30';
		this.outTime = '07:30';
		this.date = '2017-05-24';
		this.sidingQuantity = '';
		this.tiplerWeight = 0;
		this.grossWeight = 0;
		this.tierWeight = 0;
		this.netWeight = 0;
		this.notes = '';
		this.rrImage = '';
    }
}

decorate(UnloadingState, {
	rrNumber: observable,
	powerhouseName: observable,
	rakeNumber: observable,
	receivedQuantity: observable,
	numberOfBoxes: observable,
	inTime: observable,
	outTime: observable,
	date: observable,
	sidingQuantity: observable,
	tiplerWeight: observable,
	grossWeight: observable,
	tierWeight: observable,
	netWeight: observable,
	notes: observable,
	rrImage: observable,

	initialiseData: action,
})

export default new UnloadingState();

