import {observable, decorate, action} from 'mobx'


class RakeAssignmentState {

	constructor(){
		this.tpsName = '';
		this.rakeAllocate = '';
		this.fromRake = '';
		this.notes = '';
	}

    initialiseData(){
        this.tpsName = '';
		this.rakeAllocate = '';
		this.fromRake = '';
		this.notes = '';
    }
}

decorate(RakeAssignmentState, {
	tpsName: observable,
    rakeAllocate: observable,
    fromRake: observable,
    notes: observable,

	initialiseData: action,
})

export default new RakeAssignmentState();

