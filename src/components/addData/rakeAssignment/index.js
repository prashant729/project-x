import React from 'react';
import { observer } from 'mobx-react';
import RakeAssignmentState from "./rakeAssignmentState";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {rakeAssignmentCall} from "../../../API/addDataCall";
import './style.css';


const RakeAssignment = observer(class RakeAssignment extends React.Component {

	constructor(props){
        super(props);
        this.state = {
            loading: false,
            respMsg: '',
            snackbar: false,
            vertical: 'top',
			horizontal: 'center',
			inputFontSize: 30,
			labelFontSize: 20,
        }
	}

	componentDidMount(){
		RakeAssignmentState.initialiseData()
	}

	handleSubmit = () => {
		let userId = JSON.parse(sessionStorage.getItem('userDetails')).userId
		const data = {
			tpsName: RakeAssignmentState.tpsName,
			rakeAllocate: RakeAssignmentState.rakeAllocate,
			fromRake: RakeAssignmentState.fromRake,
			notes: RakeAssignmentState.notes,
			uploader: String(userId)
		}
		rakeAssignmentCall(this.rakeAssignmentCallback, data)
	}

	rakeAssignmentCallback = (response) => {
		if (response) {
			this.setState({
                loading: false,
                snackbar: true,
                respMsg: response.msg
            })
            RakeAssignmentState.initialiseData()
		}
	}

	handleClose = () => {
        this.setState({snackbar: false})
    }

    render() {
		const {loading, snackbar, vertical, horizontal, respMsg, inputFontSize, labelFontSize} = this.state
        return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">Rake Assignment</div>
                </div>
                <div className="container">
  					<div className="clearfix" />
    				<Card className="card">
      					<CardContent>
        			
							<TextField
					          	label="TPS Name"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
								value={RakeAssignmentState.tpsName}
								onChange={(e) => {
									RakeAssignmentState.tpsName = e.target.value;
								}}
					        />
					        <TextField
					          	label="Rake Allocate"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
								value={RakeAssignmentState.rakeAllocate}
								onChange={(e) => {
									RakeAssignmentState.rakeAllocate = e.target.value;
								}}
					        />
          					<TextField
					          	label="From"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
								value={RakeAssignmentState.fromRake}
								onChange={(e) => {
									RakeAssignmentState.fromRake = e.target.value;
								}}
					        />
						    <TextField
					          	label="Notes/Remarks"
					          	className="textField"
								margin="normal"
								inputProps={{
									style: {fontSize: inputFontSize} 
								}}
								InputLabelProps={{style: {fontSize: labelFontSize}}}
								value={RakeAssignmentState.notes}
								onChange={(e) => {
									RakeAssignmentState.notes = e.target.value;
								}}
					        />
      					</CardContent>
    				</Card>
    				<br/>
    				<br />
    				<Button variant="contained" color="primary" onClick={this.handleSubmit} className="textField">
				        Add Entry
				     </Button>
    			</div>
                <div className="clearfix" />
				<Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={snackbar}
                    autoHideDuration={4000}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{respMsg}</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                      ]}
                    />
            </div>
        );
    }
})

export default RakeAssignment;
