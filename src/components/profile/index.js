import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import CircularProgress from 'material-ui/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {register} from "../../API/register";
import './style.css';

const userPerm = JSON.parse(sessionStorage.getItem('userDetails')).accessPermission

class Profile extends React.Component {

	constructor(props){
        super(props);
        this.state = {
        	loading: false,
            permissions: [],
            respMsg: '',
            snackbar: false,
            vertical: 'top',
            horizontal: 'center',
        	formData: {
        		name: '',
        		email: '',
        		areaOfDuty: '1',
        		sidingName: '1',
        		accessPermission: '4',
        		password: ''
        	},
        	error: {
        		name: false,
        		email: false,
        		areaOfDuty: false,
        		sidingName: false,
        		accessPermission: false,
        		password: false
        	}
        }
    }

    componentDidMount(){
    }

	validateForm = () => {
		let errorList = this.state.error
		const formData = this.state.formData
		let readyToSubmit = true
		Object.entries(errorList).forEach(([key, value]) => {
		    if (formData[key] === '') {
		    	errorList[key] = true
		    	readyToSubmit = false
		    } else {
		    	errorList[key] = false
		    }
		});
		this.setState({error: errorList})
		if (readyToSubmit === true) {
			this.handleSubmit()
		}
	}

	handleSubmit = () => {
		this.setState({loading: true})
		let data = this.state.formData
        data.app = 'connect'
        register(this.registerCallback, data)
	}

    registerCallback = (response) => {
        if (response) {
            this.setState({
                loading: false,
                snackbar: true,
                respMsg: response.msg
            })
        }
    }

    render() {
    	const { formData, error, loading, snackbar, vertical, horizontal, respMsg, permissions } = this.state;
        return (
            <div className="full-viewport-height">
                <div className="title-bg" >
                    <div className="title-items page-title">Profile</div>
                </div>
                <div className="container">
  					
    			</div>
                <div className="clearfix" />
                <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={snackbar}
                    autoHideDuration={4000}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{respMsg}</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                      ]}
                    />
            </div>
        );
    }
}

export default Profile;
