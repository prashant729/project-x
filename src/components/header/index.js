import React, { Component } from 'react';
import Menu from 'material-ui/Menu';
import Drawer from 'material-ui/Drawer';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import LogoutIcon from 'material-ui/svg-icons/action/exit-to-app';
import {observer} from 'mobx-react';
import { Link } from 'react-router-dom';
import nucleusRoutes from "../../routes/nucleus.js";
import {some} from "underscore";

import './styles.css';

const {sessionStorage} = window;

var tabDict = {
    "broadcast_tab.R": "/broadcast",
    "members_tab.R": '/members',
    "mail_tab.R": "/cms",
    "finance_tab.R": "/finance",
    "alli_tab.R": "/alliances",
    "events_tab.R": "/events",
    "permissions_tab.R": "/permissions",
    "daily_checklist_tab.C": "/operations",
    "daily_checklist_tab.R": "/operations",
    "inventory_tab.R": "/operations"
}

var featureList = [];
if (sessionStorage.getItem('allowed_features') !== null) {
    featureList = [];
}

var allowedList = [];

for (const [key, value] of Object.entries(tabDict)) {
    if (featureList.find(function(element) {return element === key})) {
        allowedList.push(value);
    }
} 


const Header = observer(
class Header extends Component {

    constructor(){
        super();

        this.state = {
            fullName: "User",
        }
        this.styles = {
            menuIcon: {
                height: 34,
                width: 50,
                marginBottom: 10
            },
            logoutIcon: {
                height: 34,
                width: 28,
                marginTop: 15,
                marginRight: 10
            },
            leftPadding: {
                paddingLeft: 20
            },
            sidebarContent: {
                fontFamily:'Open Sans, sans-serif'
            }
        }
        this.tabList = [];
        this.doLogoutCallback = this.doLogoutCallback.bind(this);
    }

    doLogoutCallback(response){
        if(response.status){
            sessionStorage.clear();
        }else{ 
            alert(response.message)
        }
    }

    componentWillMount(){
        nucleusRoutes.map((prop, key) => {
            if (prop.restriction === false) {
                allowedList.push(prop.path);
            }
        })
        
        for (const [key, value] of Object.entries(nucleusRoutes)) {
            if (allowedList.find(function(element) {return element === value.path})) {
                    let subMenu = [];
                    if(value.subMenu){
                        for(let j=0;j<value.subMenu.length; j++){
                            let submenu = value.subMenu[j];
                            if(submenu.permissions){
                                let permissions = submenu.permissions;
                                for(let i=0; i<permissions.length; i++){
                                    let permission = permissions[i];
                                    if(some(featureList,feature => feature === permission)){
                                        subMenu.push(submenu);
                                        break;
                                    }
                                }
                            }
                            else{
                                subMenu.push(submenu);
                            }
                        }
                    }
                    this.tabList.push({
                        "path": value.path,
                        "label": value.sidebarName,
                        "subMenu": value.subMenu ? subMenu : null
                    })
            }
        }
        try{
            let profile = JSON.parse(sessionStorage.getItem('userDetails'));
        
            this.setState({fullName: profile.name });

        }catch(e){
            console.log("profile name error")
        }
    }
    handleToggle = () => this.setState({open: !this.state.open});

    handleClose = () => this.setState({open: false});
    logout(){
        sessionStorage.clear()
        window.location.reload()
    }

    render() {
        return (
            <div>
            <div className="library-icomoon custom-navbar">

                <div className="col-sm-1">

                    <MenuIcon className="icon-menu menu" onClick={this.handleToggle} style={this.styles.menuIcon} />
                    <Drawer
                        docked={false}
                        style={{backgroundColor:'red'}}
                        width={280}
                        overlayClassName="bgoverlay"
                        open={this.state.open}
                        onClose={this.handleClose}
                        onRequestChange={(open) => this.setState({open})}
                    >
                        <div style={this.styles.leftPadding}>
                            <img className="logo" src="project-logo.jpg" alt="" width={140}/>
                            <div className="login-name ">
                            <p>
                                Welcome {this.state.fullName}!
                            </p></div>
                        </div>
                        <Menu
                              onChange={this.handleClose}
                              style={this.styles.sidebarContent}
                        >

                            { 
                                this.tabList.map((key, index) => {
                                    return (
                                        !key.subMenu ? 
                                            <Link className="left-tabs"
                                                onClick={this.handleClose}
                                                key={key.label}
                                                to={key.path}>
                                                {key.label}
                                            </Link>
                                        :
                                        <div className="left-tabs"
                                            key={key.label}>
                                            {key.label}
                                            {
                                                key.subMenu.map((submenu) => {
                                                    return (
                                                        <Link to={submenu.path}
                                                            onClick={this.handleClose}
                                                            className="left-tabs left-subtabs"
                                                            key={submenu.name}>
                                                            {submenu.name}
                                                        </Link>
                                                    )
                                                })
                                            }
                                        </div>
                                    )
                                })
                            }

                        </Menu>
                        <button className="btn btn-default button-right logout-btn" onClick={this.logout.bind(this)}>Logout</button>

                    </Drawer>
                </div>
                {/*<div className="col-sm-1"><p className="admin-logo">Project X</p></div>*/}
                <div className="col-md-3 col-lg-3  text-right admin-username pull-right"><p style={{display:'inline-block',marginRight:'20px'}}>Welcome {this.state.fullName}!</p>
                    <LogoutIcon className="clickable-element" style={this.styles.logoutIcon} onClick={this.logout.bind(this)} />
                </div>
                <div className="clearfix"></div>
            </div>
            
            </div>
        );
    }
})

export default Header;
