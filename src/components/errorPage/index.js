import React, { Component } from 'react';

export default class ErrorPage extends Component {

	render(){
		return(
			<div 
				style={{
					position: 'absolute',
					top: '50%',
					left: '50%',
					transform: 'translate(-50%, -50%)',
				}}
			>
				<div>Oops! This page is not available..</div>
			</div>
		);
	}
}