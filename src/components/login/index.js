import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import { Redirect } from 'react-router-dom';

import page from '../../NavigationState';
import base64 from 'base-64';
import {login} from '../../API/login/login';
import './style.css';

var isLoggedIn = false

const {sessionStorage} = window;

export default class Login extends Component {

  constructor(){
    sessionStorage.setItem('currentpage', 'login')
    super();

    this.state = {
      // isLoggedIn: false,
      email: {
        value: '',
        valid: false,
        error: null
      },
      password: {
        value: '',
        valid: false,
        error: null
      },
      disabled: false,
      error: null,
      qrValue: 'filler',
      // loading: false
    }
  this.loginCallback = this.loginCallback.bind(this)
  this.loginerrorCallback = this.loginerrorCallback.bind(this)
  }
  
  validate(id, val){

    if(id === "email"){
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(re.test(this.state.email.value)){
        this.setState({
          email: {
            value: this.state.email.value,
            valid: true,
            error: null
          }
        })
      }else{
        this.setState({
          email: {
            value: this.state.email.value,
            valid: false,
            error: "Please enter valid email"
          }
        })
      }
    }else if(id === "pass"){
      if(this.state.password.value.length >= 8){
        this.setState({
          password: {
            value: this.state.password.value,
            valid: true,
            error: null
          }
        })
      }else{
        this.setState({
          password: {
            value: this.state.password.value,
            valid: false,
            error: "Please enter valid password"
          }
        })
      }
    }
  }

  loginCallback(){
    console.log('login callback') // do not remove this console log. it will break it
    isLoggedIn = true
    this.setState({
      disabled: true
    })
  }

  loginerrorCallback(msg){
    this.setState({
      error: msg
    })
  }

  submit(){
    this.setState({error: null});
    this.validate("email");
    this.validate("pass");
    //if(this.state.email.valid && this.state.password.valid){
      var auth = base64.encode(this.state.email.value+':'+this.state.password.value);
      login(auth, this.loginCallback, this.loginerrorCallback);
    //window.location = window.location.href;
    //}else{
      //console.log("FAILED")
      //this.setState({error: "All fields are required"});
    //}
    
  }
  

  renderLogin() {
    return ( <div className="main">
            <div className="logo-container col-xs-12 col-md-5">
                <img src="project-logo.jpg" alt=""/>
            </div>

            <div  className="col-xs-12 col-md-7" style={{ marginTop:'50px'}}>

                <div style={{position:'relative'}}>
                    <div className="qr-container col-sm-6">
                        

                    </div>


                    <div className="col-sm-6 login-container">
                        <div>
                            <TextField
                                type='email'
                                id = 'email'
                                hintText="user@email.com"
                                floatingLabelText="User ID *"
                                floatingLabelStyle={{color: 'black', fontSize: 14}}
                                errorText={this.state.email.error}
                                value={this.state.email.value}
                                floatingLabelFixed={false}
                                onChange={(event, newValue) => {
                                    this.setState({
                                        email: {
                                            value: newValue,
                                            valid: false,
                                            error: null
                                        }
                                    })
                                }}
                                onBlur={() => this.validate("email")}
                            />
                        </div>
                        <div>
                            <TextField
                                id = 'password'
                                type='password'
                                hintText="password"
                                floatingLabelText="Password *"
                                floatingLabelStyle={{color: 'black', fontSize: 14}}
                                errorText={this.state.password.error}
                                value={this.state.password.value}
                                floatingLabelFixed={false}
                                onChange={(event, newValue) => {
                                    this.setState({
                                        password: {
                                            value: newValue,
                                            valid: false,
                                            error: null
                                        }
                                    })
                                }}
                                onBlur={() => this.validate("pass")}
                            />
                        </div>
                        <div className="text-left">
                            <div style= {{color: 'red',marginTop:'20px',marginBottom:'20px'}}> { page.sessionExpired } </div>
                            <div style={{color: 'red',marginTop:'10px',marginBottom:'20px'}}>{this.state.error}</div>

                        </div>

                        <button className="btn login-btn"
                            type="submit"
                                disabled={this.state.disabled}
                                onClick={()=> this.submit()} style={{marginTop:'20px'}}
                        >SIGN IN</button>
                    </div>

                    <div className="clearfix"> </div>
                </div>


            </div>
            <div className="clearfix"></div>
        </div>
    );
  }

  render()
  { 
      if(isLoggedIn){
        return <Redirect to='/dashboard' />
      } else {
        return (this.renderLogin())
      }
  }
}

