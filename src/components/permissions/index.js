import React, { Component } from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import {Tabs, Tab} from 'material-ui/Tabs';
import Navigation from '../../NavigationState';
import Dialog from 'material-ui/Dialog';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import ImageEdit from 'material-ui/svg-icons/image/edit';
import {getallusers, updateUser, getenums, getrules, updaterules} from '../../API/permissions'

export default class Permissions extends Component {

  constructor(){
    super();
    this.state = {
      enums: {},
      centerid: '1',
      userlist: [],
      rulelist: [],
      dialog: false,
      selected_userid: 0,
      selected_username: '',
      selected_role: [],
      user_active: null,
      rule_dialog: false,
      selected_rule: {},
      create:[],
      read:[],
      update:[],
      delete:[],
    }
    this.getAllUserDetailsCallback = this.getAllUserDetailsCallback.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.submit = this.submit.bind(this);
    this.submitRule = this.submitRule.bind(this);
    this.updateUserCallback = this.updateUserCallback.bind(this);
    this.filterfunc = this.filterfunc.bind(this);
    this.getenumcallback = this.getenumcallback.bind(this);
    this.str_to_readable = this.str_to_readable.bind(this);
    this.str_to_readable_multi = this.str_to_readable_multi.bind(this);
    this.populatefield = this.populatefield.bind(this);
    this.getrulescallback = this.getrulescallback.bind(this);
    this.cellClick = this.cellClick.bind(this);
    this.convert = this.convert.bind(this);
    this.updaterulecallback = this.updaterulecallback.bind(this);
  }

  getAllUserDetailsCallback(allUserDetails){
    this.setState({userlist: allUserDetails})
  }

  getenumcallback(enums){
    this.setState({enums: enums})
  }
  
  componentDidMount(){
    getallusers(this.getAllUserDetailsCallback, this.getrulescallback)
    getenums(this.getenumcallback)
  }

  getrulescallback(rules){
    this.setState({rulelist: rules})
  }

  str_to_readable(perm){
    if(Object.keys(this.state.enums).length){
      let result = ''
      perm = JSON.parse(perm)
      for (var key in perm){
        result += (this.state.enums.groups[key] + '-' + this.state.enums.roles[perm[key]] + ', ')
      }
      return result.slice(0, -2)
  }}

  str_to_readable_multi(perm){
    if(Object.keys(this.state.enums).length){
      let result = ''
      perm = JSON.parse(perm)
      for (var key in perm){
        for (var value in perm[key]){
          result += (this.state.enums.groups[key] + '-' + this.state.enums.roles[perm[key][value]] + ', ')
        }
      }
      return result.slice(0, -2)
  }}

  openDialog(row){
    if(!row['roles']){
      row['roles'] = '{}'
    }
    let temp = JSON.parse(row['roles'])
    let role = Object.keys(temp).map(function(k) { 
      return k + '-' + temp[k]});
    this.setState({dialog: true,
      selected_userid: row['uid'],
      selected_username: row['name'],
      user_active: row['active'],
      selected_role: role
    })
  }

  populatefield(selected){
    if(Object.keys(this.state.enums).length){
      let allvalues = []
      let groups = this.state.enums.grouprole
      for (var group in groups){
        for (var role in groups[group]){
           allvalues.push(group + '-' + groups[group][role])
        } 
      }
      return allvalues.map((name) => (
        <MenuItem
        key={name}
        insetChildren={true}
        checked={selected && selected.indexOf(name) > -1}
        value={name}
        primaryText={this.state.enums.groups[name.split('-')[0]] +'-'+ this.state.enums.roles[name.split('-')[1]]}
      />
      ));
    }
  }

  updateCheck() {
    this.setState((oldState) => {
      return {
        user_active: !oldState.user_active,
      };
    });
  }

  handleClose(){
    this.setState({
      dialog: false,
      selected_userid: 0,
      selected_username: '',
      selected_role: [],
      user_active: null,
      rule_dialog: false,
      selected_rule: {},
      create:[],
      read:[],
      update:[],
      delete:[],
    })
  }

  updateUserCallback(resp){
    if(resp['status']){
        this.handleClose()
        getallusers(this.getAllUserDetailsCallback, this.getrulescallback)
    }else{
        console.log("Could not update")
    }
  }  

  filterfunc(el){
    // console.log(typeof(this.state.centerid))
    return el.center_id === parseInt(this.state.centerid)
  }   

  submit(){
    if(this.state['dialog']){
      let roles = {}
      for (var idx in this.state.selected_role){
        let key = this.state.selected_role[idx].split('-')[0]
        let value = this.state.selected_role[idx].split('-')[1]
        roles[key] = value
      }
      updateUser(this.state.selected_userid, roles, this.state.user_active, this.updateUserCallback)
    }else{
      this.handleClose()
    }
  }

  checkPerm(role, row){
    if (role !== 'undefined-Super Admin') {
      return(
        <FlatButton icon={<ImageEdit/>}
                                          onClick={() => this.openDialog(row)}/>
        )
    }
  }

  checkUndef(role){
    if (role === 'undefined-Super Admin') {
      return('Super Admin')
    } else {
      return role
    }
  }

  submitRule(){
    if(this.state['rule_dialog']){
      updaterules(this.state.selected_rule['feature'],
                  this.state.create,
                  this.state.read,
                  this.state.update,
                  this.state.delete,
                  this.updaterulecallback)
    }else{
      this.handleClose()
    }
  }

  updaterulecallback(resp){
    if(resp['status']){
      this.handleClose()
      getrules(this.getrulescallback)
    }else{
      console.log("Couldnt update rule")
    }
  }

  convert(string_group){
    if(!string_group){
      string_group = '{}'
    }
    let temp = JSON.parse(string_group)
    let result = []
    Object.keys(temp).map(function(k) { 
      for(var each in temp[k]){
        result.push(k + '-' + temp[k][each])}});
    return result
  }

  cellClick(e){
    this.setState({selected_rule: this.state.rulelist[e[0]],
                   rule_dialog: true,
                   create: this.convert(this.state.rulelist[e[0]]['create']),
                   read: this.convert(this.state.rulelist[e[0]]['read']),
                   update: this.convert(this.state.rulelist[e[0]]['update']),
                   delete: this.convert(this.state.rulelist[e[0]]['delete'])})
  }

  render() {
    // console.log(this.state)
    const actions = [
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onClick={this.submit}
      />,]
    const sendrules = [
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onClick={this.submitRule}
      />,
    ]
    let leftpane = {'height': '100vh', background: '#212121', width:'18vw'};
    return (
      <div>
        <div className="title-bg" style={{display: 'block'}} >
            <div className="title-items page-title">Permissions</div>
        </div>
        
        <div>
          <div className="col-xs-12 col-sm-6 text-center" style={leftpane}>
            <div>
              <p className='member-text'>Filter location</p>
                <DropDownMenu
                  value={this.state.centerid}
                  selectedMenuItemStyle={{color:'orange'}}
                  labelStyle={{color:'#f5f5f5',lineHeight:'normal',padding:'10px 30px 10px 10px',whiteSpace:'normal',height:'auto' }}
                  style={{height:'auto',backgroundColor:'#414141',borderRadius:'5px',}}
                  iconStyle={{right: '-8px', top: '-4px',fill:'#78909c'}}
                  underlineStyle={{display:'none'}}
                  onChange={(e, i, value)=>{
                    // Navigation.setCenter(value);
                    this.setState({centerid: value});
                  }}
                >
                {
                  Navigation.centerList.map((item, index)=>{
                    // console.log(item.center_id)
                    return(<MenuItem key={'center'+index} value={item.center_id} primaryText={item.city_name+' - '+item.center_name} />);
                  })
                }
                </DropDownMenu>
              </div>
          </div>
          
          <div className="col-xs-12 col-sm-6 text-center" style={{width: '80vw'}}>
            <div>
              <Paper style={{marginTop: 10}} zDepth={1}>
                <Tabs>
                  <Tab label="Users">
                    <Table selectable={false} height='80vh' fixedHeader={true}>
                      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                          <TableHeaderColumn>Name</TableHeaderColumn>
                          <TableHeaderColumn>Email</TableHeaderColumn>
                          <TableHeaderColumn>Group-Role</TableHeaderColumn>
                          <TableHeaderColumn>Active</TableHeaderColumn>
                          <TableHeaderColumn>Change permissions</TableHeaderColumn>
                        </TableRow>
                      </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        {this.state.userlist.filter(this.filterfunc).map( (row, index) => (
                          <TableRow key={index}>
                            <TableRowColumn>{row['name']}</TableRowColumn>
                            <TableRowColumn>{row['email']}</TableRowColumn>
                            <TableRowColumn>{this.checkUndef(this.str_to_readable(row['roles']))}</TableRowColumn>
                            <TableRowColumn>{(row['active']).toString()}</TableRowColumn>
                            <TableRowColumn>
                              {this.checkPerm(this.str_to_readable(row['roles']), row)}
                            </TableRowColumn>
                          </TableRow>
                          ))}
                      </TableBody>
                    </Table>
                  </Tab>
                  <Tab label="Rules">
                    <Table height='80vh' fixedHeader={true} onRowSelection={(e) => this.cellClick(e)}>
                      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                          <TableHeaderColumn>Name</TableHeaderColumn>
                          <TableHeaderColumn>Read</TableHeaderColumn>
                          <TableHeaderColumn>Edit</TableHeaderColumn>
                          <TableHeaderColumn>Create</TableHeaderColumn>
                          <TableHeaderColumn>Delete</TableHeaderColumn>
                        </TableRow>
                      </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        {this.state.rulelist.map( (row, index) => (
                          <TableRow key={index}>
                            <TableRowColumn style={{cursor:'pointer'}}>{row['feature']}</TableRowColumn>
                            <TableRowColumn style={{cursor:'pointer'}}>{this.str_to_readable_multi(row['read'])}</TableRowColumn>
                            <TableRowColumn style={{cursor:'pointer'}}>{this.str_to_readable_multi(row['update'])}</TableRowColumn>
                            <TableRowColumn style={{cursor:'pointer'}}>{this.str_to_readable_multi(row['create'])}</TableRowColumn>
                            <TableRowColumn style={{cursor:'pointer'}}>{this.str_to_readable_multi(row['delete'])}</TableRowColumn>
                          </TableRow>
                          ))}
                      </TableBody>
                    </Table>
                  </Tab>
                  
                </Tabs>
                
              </Paper>

              <Dialog
                title={"Change permissions for " + this.state.selected_username}
                actions={actions}
                modal={false}
                open={this.state.dialog}
                onRequestClose={() => this.handleClose()}>
                <p>Roles and groups (Multiselectable)</p>
                <SelectField
                  multiple={true}
                  hintText="Select roles and groups"
                  value={this.state.selected_role}
                  onChange={(event, index, values) => this.setState({selected_role:values})}>
                  {
                    this.populatefield(this.state.selected_role)
                  }
                </SelectField>
                <Checkbox
                  label="Account Active"
                  checked={this.state.user_active}
                  onCheck={this.updateCheck.bind(this)}
                />
                <p>NOTE: Only one role can be selected for each group</p>
              </Dialog>

              <Dialog
                autoScrollBodyContent={true}
                title={"Change groups for " + this.state.selected_rule['feature']}
                actions={sendrules}
                modal={false}
                open={this.state.rule_dialog}
                onRequestClose={() => this.handleClose()}>
                <p>Roles and groups (Multiselectable)</p>
                <p>READ</p>
                <SelectField
                  multiple={true}
                  hintText="READ"
                  value={this.state.read}
                  onChange={(event, index, values) => this.setState({read:values})}>
                  {
                    this.populatefield(this.state.read)
                  }
                </SelectField>
                <p>EDIT</p>
                <SelectField
                  multiple={true}
                  hintText="EDIT"
                  value={this.state.update}
                  onChange={(event, index, values) => this.setState({update:values})}>
                  {
                    this.populatefield(this.state.update)
                  }
                </SelectField>
                <p>UPDATE</p>
                <SelectField
                  multiple={true}
                  hintText="CREATE"
                  value={this.state.create}
                  onChange={(event, index, values) => this.setState({create:values})}>
                  {
                    this.populatefield(this.state.create)
                  }
                </SelectField>
                <p>DELETE</p>
                <SelectField
                  multiple={true}
                  hintText="DELETE"
                  value={this.state.delete}
                  onChange={(event, index, values) => this.setState({delete:values})}>
                  {
                    this.populatefield(this.state.delete)
                  }
                </SelectField>
              </Dialog>
              </div>
          </div>
          <div className='clearfix' />
        </div>
      </div>
    );
  }
}
