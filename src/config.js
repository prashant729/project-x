let env = '';

const version = 'v1';


const httpsRoot = `https://${env}api.cowrks.info/`;
const wssRoot = `wss://${env}api.cowrks.info/`;
const s3 = `https://s3.ap-south-1.amazonaws.com/${env}cdn.cowrks.com/`;

export const NUCLEUS_API = `${httpsRoot}admin`;
export const HONCHO_API = `${httpsRoot}honchos`;
export const CONNECT_API = `${httpsRoot}connect`;
export const NUCLEUS_WEB = 'http://localhost:3000';
export const AUTH_URL = `http://13.127.45.18:8101`;
// export const AUTH_URL = `http://127.0.0.1:8101`;
export const FEED_URL = `${httpsRoot}feed`;
export const DM_URL = `${httpsRoot}postman`;
export const HR_VIEW_URL = `${httpsRoot}guardian`;
export const DAYPASS_API = `${httpsRoot}daypass`;
export const HOLIDAY_API = `${httpsRoot}mrb`;
export const DM_WS_URL = `${wssRoot}postman`;
export const VMS_API = `${httpsRoot}vms`;
export const OPERATIONS_API = `${httpsRoot}operations-service/`;
export const INVENTORY_API = `${httpsRoot}inventory-management`;
export const BACK_OFFICE_SERVICE = `${httpsRoot}backoffice-service/api/${version}/cities/`;
export const ROOM_API = `${httpsRoot}backoffice-service/api/${version}/rooms`;
export const AMENITIES_API = `${httpsRoot}backoffice-service/api/${version}/room_amenities/`;
export const COMPANIES_API = `${httpsRoot}backoffice-service/api/${version}/accounts/`;
export const FLOORS_API = `${httpsRoot}backoffice-service/api/${version}/floors`
export const SAMPLE_CSV = `${s3}InventorySampleCsv.csv`;


// common
export const CMS_URL = 'https://api.cowrks.info/cms';
export const DATA_URL = 'https://api.cowrks.info/analytics';
export const INTERACT_URL = 'https://interact.cowrks.info';
export const TRAINER_URL = 'https://training.interact.cowrks.info';
