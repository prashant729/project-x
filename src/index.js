import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { createBrowserHistory } from "history/index";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { Router, Route, Switch} from "react-router-dom";
import indexRoutes from "./routes/indexRoutes.js";
const hist = createBrowserHistory();

const muiTheme= getMuiTheme({
	palette:{
		primary1Color:'#0091ea'
	}
})

ReactDOM.render(
	<MuiThemeProvider muiTheme={muiTheme}>
  	<Router history={hist}>
  	  	<Switch>
  	    	{indexRoutes.map((prop, key) => {return <Route path={prop.path} component={prop.component} key={key} />;})}
  	    </Switch>
  	  </Router>
</MuiThemeProvider>,
  document.getElementById("root")
);
