export const imageUpload = (e) => {
    return new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        var file = e.target.files[0];
        var name = file.name;
        reader.onload = function (el) {
            //Initiate the JavaScript Image object.
            var image = new Image();
            image.src = el.target.result;
            //Set the Base64 string return from FileReader as source.
            var imagedata = el.target.result.replace(
                /^data:image\/(png|jpg|jpeg);base64,/,
                ""
            );
            image.onload = function () {
                //Determine the Height and Width.
                var height = this.height;
                var width = this.width;
                // if (585 < height && height < 595 && (1075 < width && width < 1085)) {
                    let data = {
                        valid_image: true,
                        encoded_image: imagedata,
                        image_name: name
                    }
                    return resolve(data);
                // } else {
                //     return resolve({ valid_image: false });
                // }
            };
        };
    })

}