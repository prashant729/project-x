import $ from 'jquery';
import { AUTH_URL } from '../config.js'

var access_token = sessionStorage.getItem('accessToken');

export function getallusers(callback, getrulescallback) {
    $.ajax({
        url: AUTH_URL + "/users",
        headers: {
            "Authorization": access_token
        },
        data: {
            'app': 'nucleus',
            'connect_id': 'EE2SCE3'
        },
        success: function(response){
            callback(response.data)
            getrules(getrulescallback)
        },
        error: function(error){
            console.log('Couldnt get all users details')
        }
    })
}

export function updateUser(uid, grouprole, active, callback) {
    $.ajax({
        type: 'PUT',
        url: AUTH_URL + "/users/" + uid,
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify({
                    'app': 'nucleus',
                    'grouprole': grouprole,
                    'active': active
                }),
        success: function(response){
            callback({status: true})
        },
        error: function(error){
            console.log('Couldnt update users details')
        }
    })
}

export function getenums(callback) {
    $.ajax({
        type: 'GET',
        url: AUTH_URL + "/enum",
        headers: {
            "Authorization": access_token
        },
        data: {
            'filter':'permission'
        },
        success: function(response){
            callback(response.data)
        },
        error: function(error){
            console.log('Couldnt get enums')
        }
    })
}

export function getrules(callback) {
    $.ajax({
        type: 'GET',
        url: AUTH_URL + "/permissions",
        headers: {
            "Authorization": access_token
        },
        success: function(response){
            callback(response.data)
        },
        error: function(error){
            console.log('Couldnt get rules')
        }
    })
}

export function updaterules(feature, create, read, update, delete_perm, callback) {
    $.ajax({
        type: 'PUT',
        url: AUTH_URL + "/permissions",
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify({
            'feature':feature,
            'create': create,
            'read': read,
            'update': update,
            'delete': delete_perm,
        }),
        success: function(response){
            callback({status: 'true'})
        },
        error: function(error){
            console.log('Couldnt update rule')
        }
    })
}