import $ from 'jquery';
import Navigation from '../../NavigationState';
import { AUTH_URL } from '../../config.js'
const {sessionStorage} = window;

export function login(auth, callback, errorCallback) {
    $.ajax(
    {
      	url: AUTH_URL,
      	method: 'POST',
      	headers: {
        	"authorization": "Basic "+ auth,
      	},
        data: JSON.stringify({
          "app": "nucleus"
        })
    }).then(function(response) {
        sessionStorage.setItem('accessToken', response.access_token);
        sessionStorage.setItem('refreshToken', response.refresh_token);
        // alert('got tokens')
        getUserDetails(response.access_token);
        callback()
    }).catch(function(error) {
        errorCallback(error.responseJSON.msg)
    })
}

export function getUserDetails(auth) {
    $.ajax({
        url: AUTH_URL+'/users/0?self=true',
        method: 'GET',
        headers: {
            "authorization": auth,
        }
    }).then(function(response) {
        console.log('loggg', JSON.stringify(response))
        sessionStorage.setItem('userDetails', JSON.stringify(response.data));
        // sessionStorage.setItem('allowed_features', JSON.stringify(response.data.allowed_features))
        if (sessionStorage.getItem('currentpage') === 'login'){
            Navigation.initializeState()
        }
    }).catch(function(error){
        console.log('Error getting user deets', error)
    })
}