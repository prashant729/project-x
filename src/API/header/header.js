import $ from 'jquery';
import base64 from "base-64";
import AllianceState from '../../components/alliances/alliancesState';
import Navigation from '../../NavigationState';
import page from '../../NavigationState';
import { AUTH_URL } from '../../config.js'


var access_token = sessionStorage.getItem('accessToken');

export function doLogout(callback) {
	$.ajax({
		type: "GET",
		url: AUTH_URL + "/logout/",
		dataType: 'json',
		headers: {
			"Authorization": access_token
		},
		success: function (response) {
			callback(response);
		},
		error: function (error) {
			if (error.status == 401) {
				page.activePage = 'login';
				page.sessionExpired = "You've been logged out, please login again";
			}
			else {
				alert(error.message);
			}
		}
	});
}