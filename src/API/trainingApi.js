import $ from 'jquery';
import Navigation from '../NavigationState';
import base64 from 'base-64';
import { TRAINER_URL } from '../config.js'


/*
    used to fetch all the users data from the training api
 */

var accessToken = sessionStorage.getItem('accessToken');

export function getImages(user_id, center_id, callbackFunc) {
    $.ajax({
        url: TRAINER_URL + '/?user_id=' + user_id + '&center_id=' + center_id,
        method: 'GET',
        headers: {
            "Authorization": accessToken
        },
        success: function (response) {
            if (response.status) {
                // not setting access token response.access_token);
                callbackFunc(response)
            }
            if (response.message == "Unauthorized access") {
                // Navigation.activePage = 'login';
                // Navigation.sessionExpired = "You've been logged out, please login again";
            }
        },
        error: function (error) {
            console.log("error occured");
        }
    })
}

export function verifyImages(pkey, url, user_id, callbackFunc) {
    let input_data = {
        'pkey': pkey,
        'url': url,
        'user_id': user_id
    };
    $.ajax({
        url: TRAINER_URL + '/verify',
        method: 'POST',
        headers: { "Authorization": accessToken },
        data: JSON.stringify(input_data),
        success: function (response) {
            if (response.status) {
                // not setting access token response.access_token);
                callbackFunc("requestSuccess");
            }
            if (response.message == "Unauthorized access") {
                // Navigation.activePage = 'login';
                // Navigation.sessionExpired = "You've been logged out, please login again";
            }
        },
        error: function (error) {
            callbackFunc("requestFailed")
        }
    })
}

