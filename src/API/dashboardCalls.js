import $ from 'jquery';
import base64 from "base-64";
import Navigation from '../NavigationState';
import { AUTH_URL } from '../config.js'

let access_token = sessionStorage.getItem('accessToken');

export function callDataLevel1(callback) {
    $.ajax({
        method: "GET",
        url: AUTH_URL +"/overall-data/",
        headers: {
			"Authorization": access_token
		},
        success: function(response){
          	if(response){
            	callback(response)
          	}
        },
        error: function(error){
          	if (error.status == 401){

            }else {
                
            }
        }
    })
}

export function callDataLevel2(callback, areaOfDuty) {
    $.ajax({
        method: "GET",
        url: AUTH_URL +"/area-wise-data/",
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify(areaOfDuty),
        success: function(response){
            if(response){
                callback(response)
            }
        },
        error: function(error){
            if (error.status == 401){

            }else {
                
            }
        }
    })
}

export function callDataLevel3(callback, userId) {
    $.ajax({
        method: "GET",
        url: AUTH_URL +"/user-wise-data/",
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify(userId),
        success: function(response){
            if(response){
                callback(response)
            }
        },
        error: function(error){
            if (error.status == 401){

            }else {
                
            }
        }
    })
}