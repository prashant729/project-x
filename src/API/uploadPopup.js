import $ from 'jquery';
import base64 from "base-64";
import Navigation from '../NavigationState';

import { HONCHO_API } from '../config.js'


//ToDo: handle response status
export function uploadPopup(callback, payment_type, payment_method, amount, transaction_id, date, account_id, file_data, file_name, creator_name, creator_id) {
    let access_token = sessionStorage.getItem('accessToken');
    $.ajax({
        type: "POST",
        url: HONCHO_API + "/admin/upload/paymentproof/",
        dataType: 'json',
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify({
            "payment_type": payment_type,
            "payment_method": payment_method,
            "amount": amount,
            "transaction_id": transaction_id,
            "date": date,
            "account_id": account_id,
            "encoded_payslip_image": file_data,
            "payslip_name": file_name,
            "creator_name": creator_name,
            "creator_id": creator_id
        }),
        success: function (response) {
            console.log("server response :", response)
            if (response.status) {
                // not setting access token response.token.access_token);
                callback(response)
            }
        },
        error: function (error) {
            if (error.status == 401) {
                // Navigation.activePage = 'login';
                // Navigation.sessionExpired = "You've been logged out because of inactivity, please login again";
            }
        }
    })
}
