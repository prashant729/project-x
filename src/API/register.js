import $ from 'jquery';
import base64 from "base-64";
import Navigation from '../NavigationState';
import { AUTH_URL } from '../config.js'

let access_token = sessionStorage.getItem('accessToken');

export function register(callback, userData) {
    $.ajax({
        method: "POST",
        url: AUTH_URL +"/users/",
        headers: {
			"Authorization": access_token
		},
        data: JSON.stringify(userData),
        success: function(response){
          	if(response){
            	callback(response)
          	}
        },
        error: function(error){
          	if (error.status == 401){

            }else {
                
            }
        }
    })
}