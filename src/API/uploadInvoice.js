import $ from 'jquery';
import base64 from "base-64";
import Navigation from '../NavigationState';
import { HONCHO_API } from '../config.js'

//ToDo: handle response status
export function uploadInvoice(callback, company_id, file_data, file_name, data, payment_type, transaction_id, setError, setLoading) {
	let access_token = sessionStorage.getItem('accessToken');
	setLoading(true)
	$.ajax({
		type: "PUT",
		url: HONCHO_API + "/admin/upload/invoice/",
		dataType: 'json',
		headers: {
			"Authorization": access_token
		},
		data: JSON.stringify({
			"file_data": file_data,
			"file_name": file_name,
			"account_id": company_id,
			"payment_type": payment_type,
			"transaction_id": transaction_id
		}),
		success: function (response) {
			if (response.status) {
				// not setting access token response.token.access_token);
				callback(false)
			}
			setLoading(false)
			setError('something went wrong')
		},
		error: function (error) {
			setLoading(false)
			setError('something went wrong')
			if (error.status == 401) {
				// Navigation.activePage = 'login';
				// Navigation.sessionExpired = "You've been logged out because of inactivity, please login again";
			} else {

			}
		}
	})
}
