import $ from 'jquery';
import base64 from "base-64";
import { HONCHO_API } from '../config.js'

//ToDo: handle response status
export function makeAdmin(user_id, callback) {
	let access_token = sessionStorage.getItem('accessToken')
	$.ajax({
		type: "PUT",
		url: HONCHO_API + "/admin",
		dataType: 'json',
		headers: {
			"Authorization": access_token
		},
		data: JSON.stringify({
			"user_id": user_id
		}
		),
		success: function (response) {
			if (response.status) {
				callback()
			}
		},
		error: function (error) {
			if (error.status == 401) {
			} else {

			}
		}
	})
}
