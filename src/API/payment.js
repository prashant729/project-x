import $ from 'jquery';
import base64 from "base-64";
import MemberState from '../components/members/MemberState';
import Navigation from '../NavigationState';
import { HONCHO_API } from '../config.js'

//ToDo: handle response status
export function getPaymentData(callback, companyID) {
		let access_token = sessionStorage.getItem('accessToken');
		// let customUrl = configClass.honchoAPI.split('/').slice(0, -1).join('/');
		// console.log("customUrl", customUrl)
	    $.ajax({
	        type: "GET",
	        url: HONCHO_API +"/invoice/history?account_id="+companyID+"&type=all&days=30",
	        dataType: 'json',
	        headers: {
	          	"Authorization": access_token
	        },
	        success: function(response){
	          	if(response.status){
	            	// not setting access token response.token.access_token);
	            	callback(response)
	          	}
	        },
	        error: function(error){
	          	if (error.status == 401){
	                // Navigation.activePage = 'login';
	                // Navigation.sessionExpired = "You've been logged out because of inactivity, please login again";
	            }else {
	                
	            }
	        }
	    })
}
