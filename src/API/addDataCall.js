import $ from 'jquery';
import base64 from "base-64";
import Navigation from '../NavigationState';
import { AUTH_URL } from '../config.js'

let access_token = sessionStorage.getItem('accessToken');

export function loadingCall(callback, loadingData) {
    $.ajax({
        method: "POST",
        url: AUTH_URL +"/loading-data/",
        headers: {
			"Authorization": access_token
		},
        data: JSON.stringify(loadingData),
        success: function(response){
          	if(response){
            	callback(response)
          	}
        },
        error: function(error){
          	if (error.status == 401){

            }else {
                
            }
        }
    })
}

export function loadingDataById(callback, id) {
    $.ajax({
        method: "GET",
        url: AUTH_URL +"/loading-data-by-id/?id="+id,
        headers: {
			"Authorization": access_token
        },
        success: function(response){
          	if(response){
            	callback(response)
          	}
        },
        error: function(error){
          	if (error.status == 401){

            }else {
                
            }
        }
    })
}

export function unloadingDataById(callback, id) {
    $.ajax({
        method: "GET",
        url: AUTH_URL +"/unloading-data-by-id/?id="+id,
        headers: {
			"Authorization": access_token
        },
        success: function(response){
          	if(response){
            	callback(response)
          	}
        },
        error: function(error){
          	if (error.status == 401){

            }else {
                
            }
        }
    })
}

export function unloadingCall(callback, loadingData) {
    $.ajax({
        method: "POST",
        url: AUTH_URL +"/unloading-data/",
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify(loadingData),
        success: function(response){
            if(response){
                callback(response)
            }
        },
        error: function(error){
            if (error.status == 401){

            }else {
                
            }
        }
    })
}

export function rakeAssignmentCall(callback, rakeAssignmentData) {
    $.ajax({
        method: "POST",
        url: AUTH_URL +"/rake-assignment/",
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify(rakeAssignmentData),
        success: function(response){
            if(response){
                callback(response)
            }
        },
        error: function(error){
            if (error.status == 401){

            }else {
                
            }
        }
    })
}

export function rakePositionCall(callback, rakePositionData) {
    $.ajax({
        method: "POST",
        url: AUTH_URL +"/rake-position/",
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify(rakePositionData),
        success: function(response){
            if(response){
                callback(response)
            }
        },
        error: function(error){
            if (error.status == 401){

            }else {
                
            }
        }
    })
}

export function placementCall(callback, loadingData) {
    $.ajax({
        method: "POST",
        url: AUTH_URL +"/indent-placement-data/",
        headers: {
            "Authorization": access_token
        },
        data: JSON.stringify(loadingData),
        success: function(response){
            if(response){
                callback(response)
            }
        },
        error: function(error){
            if (error.status == 401){

            }else {
                
            }
        }
    })
}