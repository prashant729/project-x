import {autorun} from 'mobx';
const {sessionStorage} = window;

class MainState {

	initializeState(){
		if(sessionStorage.getItem('currentpage') === 'login'){
			window.location.reload()
		}
	}
}


var mainStore = new MainState();

autorun(()=>{
	if (sessionStorage.getItem('currentpage') === 'login'){
		sessionStorage.setItem('currentpage', 'dashboard')
		mainStore.initializeState()
	}
	
})

export default mainStore;
