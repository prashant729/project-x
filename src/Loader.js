'use strict'

import React, { Component } from 'react';
import { observer } from 'mobx-react';
import BookingState from './booking/BookingState';
import CircularProgress from 'material-ui/CircularProgress';
// import Navigation from '../NavigationState';

const Loading = observer( class Loading extends Component{

	render(){
		if (BookingState.dataLengthCheck) {
			return(

					<div className="loading-content">{BookingState.dataLengthCheck}</div>
				)
		}
		else{
			return(
				<div className="vhalign">
					 <CircularProgress size={60} thickness={4}/>
				</div>
			);
		}
	}
})

export default Loading;
